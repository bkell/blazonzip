#!/usr/bin/env python3

import multiprocessing
import sys

import cv2
import numpy as np

import images
import population


def print_best_sse(population):
    print(
        'Best SSE (' + population.name + '): ' +
        str(population.candidates[0].sse()),
        end='')
    if population.unchanged > 0:
        print(
            ' (unchanged for ' + str(population.unchanged) + ' generation' +
            ('' if population.unchanged == 1 else 's') + ')',
            end='')
    elif population.previous_best_sse is not None:
        print(
            ' ({0:0.6%})'.format(
                (population.candidates[0].sse() - population.previous_best_sse)
                / population.previous_best_sse),
            end='')
    print()


def main():
    # https://bugs.python.org/issue33725
    if sys.platform == 'darwin': multiprocessing.set_start_method('forkserver')

    if len(sys.argv) != 2:
        print('Usage: blazonzip.py image')
        sys.exit(1)

    # Check to make sure DRAWSHIELD_DIR is defined. (Otherwise the exception is
    # raised inside a child process, a line is written to blazonzip-error.log,
    # and the loop continues.)
    images.get_drawshield_dir()

    target_pixels = population.file_to_pixels(sys.argv[1])
    cv2.imshow('Target', target_pixels.astype('uint8'))
    cv2.namedWindow('Best blazon')
    cv2.namedWindow('Diff')
    # This pause (1 ms) is needed to give the image window time to repaint (?).
    cv2.waitKey(1)
    metropole = population.Population('metropole', target_pixels)
    colony = population.Population('colony', target_pixels)
    last_metropole_sse = None
    last_colony_sse = None
    while True:
        if (colony.candidates[0].sse() < metropole.candidates[1].sse() or
                colony.generation >= 100):
            print("Integrating colony.")
            metropole.integrate(colony)
            start_colony_fresh = True
        else:
            start_colony_fresh = False
        print('Best blazon:', metropole.candidates[0].ast_root.blazon())
        print_best_sse(metropole)
        print_best_sse(colony)
        cv2.imshow('Best blazon',
                   metropole.candidates[0].pixels().astype('uint8'))
        cv2.imshow(
            'Diff',
            np.absolute(metropole.target_pixels - metropole.candidates[0]
                        .pixels()).astype('uint8'))
        cv2.waitKey(1)
        metropole.repopulate()
        colony.repopulate(start_fresh=start_colony_fresh)


if __name__ == '__main__':
    main()
