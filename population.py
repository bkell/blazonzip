import bisect
from datetime import datetime
import itertools
import math
from multiprocessing import Pool
import random
from threading import Condition
import traceback

from tqdm import tqdm  # https://github.com/tqdm/tqdm or package python3-tqdm

import ast
import images

# DrawShield SVGs have a native resolution of 500 x 600.
_EVAL_WIDTH = 50
_EVAL_HEIGHT = 60

# Our NumPy ndarrays of pixels have dtype int32, so we need to make sure that
# the maximum possible sum of squared errors does not exceed int32max.
assert 255 * 255 * 3 * _EVAL_WIDTH * _EVAL_HEIGHT <= 2147483647


def file_to_pixels(filename):
    return images.file_to_pixels(filename, _EVAL_WIDTH, _EVAL_HEIGHT)


def select(seq):
    """Returns a random element, weighted toward the head.

    This uses the following continuous linear probability distribution, having
    support [0, N), where N is the length of the given sequence.
        pdf: f(x) = 2/N - 2x/N^2
        cdf: F(x) = 2x/N - x^2/N^2
        inverse cdf: F^{-1}(y) = N(1 - sqrt(1 - y))
    A random number is chosen according to this distribution, and the floor is
    used as an index into the sequence. Statistics about this distribution:
        y      F^{-1}(y)
        0.50   0.2929 N
        0.80   0.5528 N
        0.90   0.6838 N
        0.95   0.7764 N
        0.98   0.8586 N
        0.99   0.9000 N
    So, for example, 90% of the time the index will be less than 0.6838 N.
    """
    n = len(seq)
    if n < 1: raise IndexError('Cannot select from an empty sequence')
    return seq[math.floor(n * (1 - math.sqrt(1 - random.random())))]


def log_exception(filename, e):
    with open(filename, 'a') as log:
        log.write('===== ' + str(datetime.now()) + ' =====\n')
        traceback.print_exception(None, e, None, file=log)
        log.write('\n\n')


class Candidate:
    """A wrapper around an AST that caches the rendered pixels and error score.

    Attributes:
        ast_root: The root of the AST.
        _pixels_memoized: The rendered pixels, as a NumPy ndarray with dtype
            int32 but values in the uint8 range (0-255).
        _sse_memoized: The sum of squared pixel errors between the target image
            and the rendered image.
    """

    def __init__(self, ast_root=None):
        if ast_root is None:
            self.ast_root = ast.random_ast()
        else:
            self.ast_root = ast_root
        # These aren't computed right away because they're expensive and we
        # might not want to use this candidate (if it's identical to another
        # candidate).
        self._pixels_memoized = None
        self._sse_memoized = None

    def compute_pixels_and_sse(self, target_pixels):
        blazon = self.ast_root.blazon()
        try:
            self._pixels_memoized = images.png_to_pixels(
                images.svg_to_png(
                    images.blazon_to_svg(blazon), _EVAL_WIDTH, _EVAL_HEIGHT))
            self._sse_memoized = images.sse(target_pixels,
                                            self._pixels_memoized)
        except Warning as w:
            raise Warning('Blazon: ' + blazon) from w
        except Exception as e:
            raise Exception('Blazon: ' + blazon) from e
        return self

    def pixels(self):
        assert self._pixels_memoized is not None
        return self._pixels_memoized

    def sse(self):
        assert self._sse_memoized is not None
        return self._sse_memoized


class Population:
    """A population of candidate blazons, where the evolution takes place.

    Attributes:
        name: The name of this population.
        target_pixels: The target image as a NumPy ndarray of pixels, with dtype
            int32 but values in the uint8 range (0-255).
        population_size: The number of candidates in each generation.
        generation: The number of the current generation, starting at 1.
        candidates: The list of candidate blazons, in order by error score.
        previous_best_sse: The best SSE in the previous generation.
        unchanged: The number of generations for which the best SSE has not
            changed.
        tng: The next generation of candidates, as it is being built.
        tng_structures: The set of structures of the candidates that have been
            evaluated for tng. This includes structures for candidates that
            cause errors in DrawShield or SVG rendering, so that they won't be
            tried again.
    """

    def __init__(self, name, target_pixels, population_size=100):
        self.name = name
        self.target_pixels = target_pixels
        self.population_size = population_size
        self.generation = 0
        self.candidates = None
        self.previous_best_sse = None
        self.unchanged = None

        # condition guards tng, tng_structures, in_flight, and progress_bar.
        self.condition = Condition()
        self.tng = []
        self.tng_structures = set()
        self.in_flight = 0
        self.progress_bar = None

        self.repopulate(start_fresh=True)

    def consider_candidate(self, candidate, pool, is_newly_constructed=True):
        """Considers a candidate for inclusion in tng.

        The candidate's AST is first simplified.

        A candidate will not be accepted if it has the same structure as a
        candidate that has already been considered for tng.

        If the candidate's structure is new, this function also computes its
        pixels and sum of squared errors. The candidate will not be accepted if
        it causes errors in DrawShield or SVG rendering.

        If the candidate passes all of these steps, it is accepted and appended
        to tng.

        This function does not acquire self.condition; the caller is expected to
        do that.
        """
        if is_newly_constructed: candidate.ast_root.simplify()
        structure = candidate.ast_root.structure()
        if structure in self.tng_structures: return
        self.tng_structures.add(structure)
        if is_newly_constructed:
            pool.apply_async(
                candidate.compute_pixels_and_sse,
                args=(self.target_pixels, ),
                callback=self.success_callback,
                error_callback=self.error_callback)
            self.in_flight += 1
        else:
            self.tng.append(candidate)
            self.progress_bar.update(1)

    def success_callback(self, candidate):
        with self.condition:
            self.tng.append(candidate)
            self.in_flight -= 1
            self.progress_bar.update(1)
            self.condition.notify()

    def error_callback(self, e):
        if isinstance(e, Warning):
            log_exception('blazonzip-warning.log', e)
        else:
            log_exception('blazonzip-error.log', e)
        with self.condition:
            self.in_flight -= 1
            self.condition.notify()

    def repopulate(self, start_fresh=False):
        """Produces the next generation of candidates.

        Args:
            start_fresh: If true, the new generation will be completely random,
                with no relation to the previous generation; the generation
                number is reset to 1 and information about previous best SSE is
                discarded. This is used for the first generation and for
                restarting the colony.
        """
        if start_fresh:
            self.generation = 1
            self.previous_best_sse = None
        else:
            self.generation += 1
            self.previous_best_sse = self.candidates[0].sse()
        self.tng = []
        self.tng_structures = set()
        self.progress_bar = tqdm(
            total=self.population_size,
            desc='{0}, generation {1:d}'.format(self.name.capitalize(),
                                                self.generation))
        with Pool() as pool:
            with self.condition:
                while len(self.tng) < self.population_size:
                    while (len(self.tng) + self.in_flight <
                           self.population_size):
                        r = random.random()
                        if start_fresh:
                            # Generate a new candidate from scratch.
                            self.consider_candidate(Candidate(), pool)
                        elif not self.tng:
                            # Preserve current best candidate.
                            self.consider_candidate(
                                self.candidates[0],
                                pool,
                                is_newly_constructed=False)
                        elif r < 0.1:
                            # Copy a candidate from the previous generation.
                            self.consider_candidate(
                                select(self.candidates),
                                pool,
                                is_newly_constructed=False)
                        elif (r < 0.6 and len(self.tng) + self.in_flight <=
                              self.population_size - 2):
                            # Crossover.
                            mother = select(self.candidates)
                            father = select(self.candidates)
                            for offspring in self.crossover(mother, father):
                                self.consider_candidate(offspring, pool)
                        else:
                            # Mutation.
                            self.consider_candidate(
                                self.mutate(select(self.candidates)), pool)
                    self.condition.wait()
            pool.close()
            pool.join()
        assert len(self.tng) == self.population_size
        self.progress_bar.close()
        self.tng.sort(key=lambda c: c.sse())
        self.candidates = self.tng
        if (self.previous_best_sse is None or
                self.candidates[0].sse() != self.previous_best_sse):
            self.unchanged = 0
        else:
            self.unchanged += 1

    def integrate(self, other_population):
        """Integrates another population into this one.

        The two populations are merged and sorted by SSE. The other population
        is not modified.

        This does not count as a generation.
        """
        structures = {
            candidate.ast_root.structure()
            for candidate in self.candidates
        }
        for candidate in other_population.candidates:
            structure = candidate.ast_root.structure()
            if structure not in structures:
                self.candidates.append(candidate)
                structures.add(structure)
        self.candidates.sort(key=lambda c: c.sse())
        if (self.unchanged is not None and self.unchanged > 0 and
                self.candidates[0].sse() != self.previous_best_sse):
            self.unchanged = 0

    def mutate(self, source_candidate):
        """Produces a mutation of a candidate.

        The mutation is done by re-expanding a random AST node.

        Args:
            source_candidate: A Candidate object whose AST will be used as the
                source. This candidate itself will not be modified.

        Returns:
            A mutated copy of the given candidate.
        """
        mutated_ast_root = ast.AstNode(copy_from=source_candidate.ast_root)
        mutated_ast_root.random_node().expand()
        return Candidate(ast_root=mutated_ast_root)

    def crossover(self, mother_candidate, father_candidate):
        """Performs a crossover operation on two candidates.

        Crossover takes two ASTs, finds a pair of internal (nonroot,
        nonterminal) nodes (one in each AST) that have the same token, and swaps
        the subtrees rooted at those nodes. Such a pair of nodes always exists,
        because every blazon has at least one field.

        Args:
            mother_candidate: The first source candidate. The source candidates
                themselves will not be modified.
            father_candidate: The second source candidate.

        Returns:
            The results of the crossover as a 2-tuple of candidates.
        """
        clones = (ast.AstNode(copy_from=mother_candidate.ast_root),
                  ast.AstNode(copy_from=father_candidate.ast_root))

        # Iterate through the internal (nonroot, nonterminal) nodes of both ASTs
        # to build a dictionary keyed by token, where each value is a 2-tuple of
        # lists (one each for the mother and the father) containing nodes
        # corresponding to that token.
        token_map = {}
        for clone_number in range(2):

            def visitor(node):
                if node.parent is not None and not node.is_terminal:
                    entry = token_map.setdefault(node.token, ([], []))
                    entry[clone_number].append(node)

            clones[clone_number].visit(visitor)

        # Choose a random token (key) from this dictionary, weighted by the
        # product of the lengths of the lists.
        tokens = []
        weights = []
        for token, node_lists in token_map.items():
            weight = len(node_lists[0]) * len(node_lists[1])
            if weight > 0:
                tokens.append(token)
                weights.append(weight)
        assert tokens
        cumulative_weights = list(itertools.accumulate(weights))
        x = random.random() * cumulative_weights[-1]
        token = tokens[bisect.bisect(cumulative_weights, x)]

        # For the selected token, choose a random node from each list.
        node_lists = token_map[token]
        mother_node = random.choice(node_lists[0])
        father_node = random.choice(node_lists[1])

        # Swap the corresponding subtrees.
        mother_node_parent = mother_node.parent
        mother_node_offset = mother_node.sibling_offset
        father_node_parent = father_node.parent
        father_node_offset = father_node.sibling_offset
        mother_node_parent.replace_subtree(mother_node_offset, father_node)
        father_node_parent.replace_subtree(father_node_offset, mother_node)

        return (Candidate(ast_root=clones[0]), Candidate(ast_root=clones[1]))
