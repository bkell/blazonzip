import io
import os
import re
import subprocess
import sys

import cairo
import cv2
from gi import require_version
require_version('Rsvg', '2.0')
from gi.repository import Rsvg
import numpy as np

# Highly reliable regex XML parsing.
DRAWSHIELD_ERROR_PATTERN = re.compile(
    b'<blazonML:message [^>]*category="blazon"[^>]*?(?: context="([^"]*)")?'
    b'[^>]*>(.*)</blazonML:message>')
DRAWSHIELD_WARNING_PATTERN = re.compile(
    b'<blazonML:message [^>]*category="[Ww]arning"[^>]*>(.*)'
    b'</blazonML:message>')


def get_drawshield_dir():
    drawshield_dir = os.environ.get('DRAWSHIELD_DIR')
    if drawshield_dir is None:
        raise Exception(
            'DRAWSHIELD_DIR environment variable not defined. Please define '
            'DRAWSHIELD_DIR with the path containing drawshield.php (without '
            'the file name). Get DrawShield from '
            'https://github.com/karlwilcox/Drawshield-Code')
    return drawshield_dir


def blazon_to_svg(blazon):
    drawshield_dir = get_drawshield_dir()
    drawshield_timeout_seconds = 300
    # DrawShield requires PHP with the XML module. In most Linux distributions,
    # the necessary package is called php-xml.
    drawshield_output = subprocess.run(
        ['php', 'drawshield.php', blazon, 'drawn in a flat style'],
        check=True,
        cwd=drawshield_dir,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
        timeout=drawshield_timeout_seconds)
    if drawshield_output.stderr != b'':
        raise RuntimeError('DrawShield wrote to STDERR: ' +
                           drawshield_output.stderr.decode('utf-8'))
    svg = drawshield_output.stdout
    error_match = DRAWSHIELD_ERROR_PATTERN.search(svg)
    if error_match:
        if error_match.group(1) is None:
            context = ''
        else:
            context = ' ' + error_match.group(1).decode('utf-8')
        raise RuntimeError('DrawShield error: ' + error_match.group(2).decode(
            'utf-8') + context)
    warning_matches = DRAWSHIELD_WARNING_PATTERN.findall(svg)
    warnings = []
    for match in warning_matches:
        warning_text = match.decode('utf-8')
        # Ignore "Features can only be plain colours", which seems to be
        # spurious: e.g., "Argent, a wolf head vair" produces this warning, but
        # it does indeed produce a wolf head vair.
        if warning_text != 'Features can only be plain colours':
            warnings.append(warning_text)
    if len(warnings) == 1:
        raise RuntimeWarning('DrawShield warning: ' + warnings[0])
    if warnings:
        raise RuntimeWarning('DrawShield warnings: ' + ';'.join(warnings))
    return svg


def svg_to_png(svg_data, width, height):
    """Renders the given SVG data as PNG data.

    Args:
        svg_data: An SVG document as bytes (as emitted by DrawShield).
        width: The desired width of the PNG.
        height: The desired height of the PNG.

    Returns:
        The rendered image as bytes, in PNG format.
    """
    handle = Rsvg.Handle.new_from_data(svg_data)
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)
    context = cairo.Context(surface)
    # Draw a white rectangle for the background. Otherwise the semitransparent
    # parts of the SVG show up weirdly when imported as a NumPy array (which
    # doesn't have an alpha channel).
    context.rectangle(0, 0, width, height)
    context.set_source_rgb(1, 1, 1)
    context.fill()
    # This scaling is applied to future operations on the context, i.e., the SVG
    # rendering.
    context.scale(width / handle.props.width, height / handle.props.height)
    handle.render_cairo(context)
    png_io = io.BytesIO()
    surface.write_to_png(png_io)
    return png_io.getvalue()


def png_to_pixels(png_data):
    """Converts the given PNG data into a NumPy ndarray of pixels.

    Args:
        png_data: Bytes containing an image in PNG format.

    Returns:
        A NumPy ndarray of pixels.

        The shape of the returned ndarray is (height, width, 3). The three
        coordinates in the third dimension are blue, green, red.

        The dtype of the returned ndarray is int32, but the values are in the
        uint8 range (0-255). Using int32 as the dtype means that subtraction
        works correctly, but it also means that the NumPy array is not suitable
        for passing to cv2.imshow. For that you will need to convert the dtype
        back to uint8.
    """
    return cv2.imdecode(
        np.asarray(
            bytearray(png_data), dtype='uint8'),
        cv2.IMREAD_COLOR).astype('int32')


def file_to_pixels(path, width, height):
    """Reads an image file and returns a NumPy ndarray of pixels.

    Args:
        path: The file path to read. This can be absolute or relative.
        width: The desired width of the ndarray.
        height: The desired height of the ndarray.

    Returns:
        A NumPy ndarray of pixels.

        The shape of the returned ndarray is (height, width, 3). The three
        coordinates in the third dimension are blue, green, red.

        The dtype of the returned ndarray is int32, but the values are in the
        uint8 range (0-255). Using int32 as the dtype means that subtraction
        works correctly, but it also means that the NumPy array is not suitable
        for passing to cv2.imshow. For that you will need to convert the dtype
        back to uint8.
    """
    return cv2.resize(
        cv2.imread(path, cv2.IMREAD_COLOR), (width, height),
        interpolation=cv2.INTER_AREA).astype('int32')


def show_pixels(pixels):
    cv2.imshow('image', pixels)
    # Wait for Q to be pressed or the window to be closed.
    while (cv2.waitKey(1) & 0xff not in [ord('Q'), ord('q')] and
           cv2.getWindowProperty('image', cv2.WND_PROP_VISIBLE) >= 1):
        pass
    cv2.destroyAllWindows()


def sse(pixels_a, pixels_b):
    return np.sum(np.square(pixels_a - pixels_b))
