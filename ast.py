import random

import grammar


def blazon(*args):
    """Returns the (partial) blazon corresponding to one or more AST nodes.

    If more than one node is provided, the corresponding partial blazons are
    concatenated.
    """
    tokens = []
    stack = list(reversed(args))
    while stack:
        node = stack.pop()
        if node.is_terminal:
            tokens.append(node.token)
        else:
            stack.extend(reversed(node.children))
    return ' '.join(tokens)


class AstNode:
    """A node in the AST, representing a particular expansion of the grammar.

    Attributes:
        token: The token in the grammar to which this node corresponds. This can
            be either a terminal or a nonterminal.
        is_terminal: A boolean indicating whether this node corresponds to a
            terminal in the grammar.
        node_count: The number of nodes in the subtree rooted at this node,
            counting this node.
        terminal_count: The number of terminal nodes in the subtree rooted at
            this node. Note that there is a difference between a terminal and a
            leaf node in the AST: a token with expansions in the grammar is not
            a terminal, even if the chosen expansion of that token happens to be
            empty (so the node has no children). So terminal_count might be
            zero, which means that this subtree will not contribute tokens to
            the blazon.
        parent: The parent of this node, or None if this is the root of the AST.
        sibling_offset: The offset of this node in its parent's child list.
        children: The list of child nodes. This is empty if is_terminal is True.
            (It might also be empty even if is_terminal is False, if the chosen
            expansion of this node's token happens to be empty.)
    """

    def __init__(self,
                 parent=None,
                 sibling_offset=None,
                 expand_token=None,
                 copy_from=None):
        assert (parent is None) == (sibling_offset is None)
        self.parent = parent
        self.sibling_offset = sibling_offset
        if expand_token is not None:
            assert copy_from is None
            self.token = expand_token
            self.expand(update_ancestors=False)
        elif copy_from is not None:
            self.token = copy_from.token
            self.is_terminal = copy_from.is_terminal
            self.node_count = copy_from.node_count
            self.terminal_count = copy_from.terminal_count
            self.children = [
                AstNode(
                    parent=self, sibling_offset=offset, copy_from=child)
                for offset, child in enumerate(copy_from.children)
            ]
        else:
            raise Exception("AstNode requires expand_token or copy_from")

    def expand(self, update_ancestors=True):
        if update_ancestors:
            old_node_count = self.node_count
            old_terminal_count = self.terminal_count
        grammar_node = grammar.GRAMMAR.get(self.token)
        if grammar_node is None:
            self.is_terminal = True
            self.children = []
            self.node_count = 1
            self.terminal_count = 1
        else:
            self.is_terminal = False
            self.children = [
                AstNode(
                    parent=self,
                    sibling_offset=offset,
                    expand_token=expansion_token)
                for offset, expansion_token in enumerate(
                    grammar_node.random_expansion())
            ]
            self.node_count = 1 + sum(
                map(lambda child: child.node_count, self.children))
            self.terminal_count = sum(
                map(lambda child: child.terminal_count, self.children))
        if update_ancestors:
            node_count_delta = self.node_count - old_node_count
            terminal_count_delta = self.terminal_count - old_terminal_count
            node_to_fix = self.parent
            while node_to_fix is not None:
                node_to_fix.node_count += node_count_delta
                node_to_fix.terminal_count += terminal_count_delta
                node_to_fix = node_to_fix.parent

    def random_node(self):
        """Returns a uniformly random node in the subtree rooted here."""
        # Depth-first, preorder number of a node.
        node_number = random.randrange(self.node_count)
        node = self
        while node_number > 0:
            assert node_number < node.node_count
            node_number -= 1
            found_child = False
            for child in node.children:
                if node_number < child.node_count:
                    node = child
                    found_child = True
                    break
                else:
                    node_number -= child.node_count
            if not found_child:
                raise Exception("This shouldn't happen: ran out of nodes")
        return node

    def visit(self, func):
        """Applies a given function at every node in the subtree rooted here.

        This is a preorder tree traversal.

        Args:
            func: A function to be called at every node of the subtree rooted at
                this node.
        """
        func(self)
        for child in self.children:
            child.visit(func)

    def replace_subtree(self, child_offset, new_subtree_root):
        assert 0 <= child_offset < len(self.children)
        node_count_delta = (new_subtree_root.node_count -
                            self.children[child_offset].node_count)
        terminal_count_delta = (new_subtree_root.terminal_count -
                                self.children[child_offset].terminal_count)
        self.children[child_offset] = new_subtree_root
        new_subtree_root.parent = self
        new_subtree_root.sibling_offset = child_offset
        node_to_fix = self
        while node_to_fix is not None:
            node_to_fix.node_count += node_count_delta
            node_to_fix.terminal_count += terminal_count_delta
            node_to_fix = node_to_fix.parent

    def blazon(self):
        return blazon(self)

    def structure(self):
        if self.is_terminal:
            return self.token
        else:
            return self.token + '(' + '+'.join(
                [child.structure() for child in self.children]) + ')'

    def _token_from_terminal_child(self, assert_token=None):
        """Returns the token of this node's only child, which is a terminal.

        It is common to have a token in the grammar that has many expansions,
        all of which are a single terminal token. This function will return that
        terminal token, after some basic assertions.

        Args:
            assert_token: The expected token of this node.
        """
        if assert_token is not None: assert self.token == assert_token
        assert len(self.children) == 1
        child = self.children[0]
        assert child.is_terminal
        return child.token

    def simplify(self):
        """Applies some simplifications to the AST.

        These simplifications clean up some undesirable things in the blazon and
        prevent it from getting too ridiculously unwieldy for no reason.

        These simplifications are highly dependent on the particular structure
        of the grammar, so if the grammar is restructured or extended the
        simplifications might have to be updated.
        """
        for child in self.children:
            child.simplify()
        self._simplify_binary_treatment()
        self._simplify_ordinaries()
        self._simplify_charges()

    def _simplify_binary_treatment(self):
        """Simplifies, e.g., "gules maily gules" to just "gules"."""
        if self.token != 'FILL': return
        assert self.children
        treatment = self.children[0]
        if treatment.token != 'TREATMENT': return
        assert len(treatment.children) >= 2
        if treatment.children[1].token != 'BINARY_TREATMENT': return
        assert len(treatment.children) == 3
        left = treatment.children[0]
        right = treatment.children[2]
        if (left._token_from_terminal_child(assert_token='TINCTURE_OR_FUR') ==
                right._token_from_terminal_child(
                    assert_token='TINCTURE_OR_FUR')):
            self.replace_subtree(0, left)

    def _simplify_ordinaries(self):
        """Simplifies a list of ordinaries.

        The same ordinary should not appear more than once in the same list,
        because earlier instances of the ordinary will be covered by later
        instances. So this function deletes the earlier instances. For example,
        "a bend azure, a chief sable, a bend gules" has two instances of "bend",
        so it will be simplified to "a chief sable, a bend gules".
        """
        # Ignore unless this is the first ORDINARIES node in the list.
        if self.token != 'ORDINARIES' or self.parent.token == 'ORDINARIES':
            return
        # Walk down the list and build a stack of the ORDINARIES nodes (except
        # the empty one at the end).
        stack = []
        node = self
        while node.children:
            assert len(node.children) == 2
            stack.append(node)
            node = node.children[1]
            assert node.token == 'ORDINARIES'
        # Now come back up the list, starting at the last node, and delete
        # duplicates.
        ordinaries = set()
        while stack:
            node = stack.pop()
            ordinary_with_fill = node.children[0]
            assert ordinary_with_fill.token == 'ORDINARY_WITH_FILL'
            assert 2 <= len(ordinary_with_fill.children) <= 3
            an_ordinary = ordinary_with_fill.children[0]
            assert an_ordinary.token in ['AN_ORDINARY', 'AN_ORDINARY_NO_FILL']
            ordinary = an_ordinary.blazon()
            if ordinary in ordinaries:
                # This ordinary already appears later in the list. Delete this
                # node by replacing it with its child ORDINARIES node.
                assert node.parent is not None
                node.parent.replace_subtree(node.sibling_offset,
                                            node.children[1])
            else:
                ordinaries.add(ordinary)

    def _simplify_charges(self):
        """Simplifies a list of charges.

        The same charge, with the same position, number, and arrangement, should
        not appear more than once in the same list, because earlier instances
        will be covered by later instances. So this function deletes the earlier
        instances. For example, "two lozenges vert, a lozenge gules, a lion
        rampant or, two lozenges sable" has two instances of "two lozenges", so
        it will be simplified to "a lozenge gules, a lion rampant or, two
        lozenges sable". Note that "a lozenge" and "two lozenges" are not
        duplicates.
        """
        # Ignore unless this is the first CHARGES node in the list.
        if self.token != 'CHARGES' or self.parent.token == 'CHARGES': return
        # Walk down the list and build a stack of the CHARGES nodes (except the
        # empty one at the end).
        stack = []
        node = self
        while node.children:
            assert len(node.children) == 4
            stack.append(node)
            assert node.children[3].token == 'CHARGES', ' + '.join(
                [child.token for child in node.children])
            node = node.children[3]
        # Now come back up the list, starting at the last node, and delete
        # duplicates.
        qualified_charges = set()
        while stack:
            node = stack.pop()
            position = node.children[0]
            assert position.token == 'CHARGE_POSITION_OPT'
            charge_arrangement = node.children[1]
            assert charge_arrangement.token == 'CHARGE_ARRANGEMENT'
            assert len(charge_arrangement.children) >= 1
            if (charge_arrangement.children[0].token in
                ['A_CHARGE', 'A_CHARGE_NO_FILL']):
                # A_CHARGE + FILL
                # or
                # A_CHARGE_NO_FILL
                qualified_charge = blazon(position,
                                          charge_arrangement.children[0])
            else:
                assert len(charge_arrangement.children) >= 3
                if (charge_arrangement.children[2].token.startswith(
                        'ARRANGEMENT_')):
                    # n + CHARGE_PLURAL + ARRANGEMENT_n + FILL
                    # or
                    # n + CHARGE_PLURAL_NO_FILL + ARRANGEMENT_n
                    qualified_charge = blazon(
                        position, *charge_arrangement.children[1:3])
                elif charge_arrangement.children[1].token == 'CHARGE_PLURAL':
                    # n + CHARGE_PLURAL + FILL + ',' + COMPOSITION_n + ';'
                    assert len(charge_arrangement.children) == 6
                    assert charge_arrangement.children[4].token.startswith(
                        'COMPOSITION_')
                    qualified_charge = blazon(
                        position, *charge_arrangement.children[1:5:3])
                else:
                    # n + CHARGE_PLURAL_NO_FILL + ',' + COMPOSITION_n + ';'
                    assert len(charge_arrangement.children) == 5
                    assert (charge_arrangement.children[1].token ==
                            'CHARGE_PLURAL_NO_FILL')
                    assert charge_arrangement.children[3].token.startswith(
                        'COMPOSITION_')
                    qualified_charge = blazon(
                        position, *charge_arrangement.children[1:4:2])
            if qualified_charge in qualified_charges:
                # This charge (and number) already appears later in the list.
                # Delete this node by replacing it with its child CHARGES node.
                assert node.parent is not None
                replacement = node.children[3]
                assert replacement.token == 'CHARGES'
                node.parent.replace_subtree(node.sibling_offset, replacement)
            else:
                qualified_charges.add(qualified_charge)


def random_ast():
    return AstNode(expand_token=grammar.GRAMMAR_START_TOKEN)
