"""Context-free grammar to generate random blazons for DrawShield.

This grammar is strongly tailored for DrawShield and its quirks (e.g., its
parser and the kinds of things it knows how to draw). It's questionable whether
the blazonry produced by this grammar is useful or meaningful in non-DrawShield
contexts. (And if DrawShield changes, like maybe by switching to a new
Flex/Bison parser[1], then lots of stuff here might have to be changed too.) In
any case, this grammar does not even attempt to handle things like proper
capitalization and punctuation, so expect to have to do some aesthetic touch-up
by hand at the end.

[1] https://github.com/karlwilcox/Blazon-Parser
"""

import random


class GrammarNode:
    """An internal node (that is, a nonterminal) in the grammar.

    Attributes:
        expansions: The list of possible expansions (lists of child nodes). The
            same expansion may appear multiple times, which affects the
            probability that it is chosen.
    """

    def __init__(self, *args):
        self.expansions = []
        for expansion in args:
            if not isinstance(expansion, list):
                expansion = [expansion]
            if expansion and isinstance(expansion[0], int):
                multiplier = expansion.pop(0)
            else:
                multiplier = 1
            for _ in range(multiplier):
                self.expansions.append(expansion)

    def random_expansion(self):
        return random.choice(self.expansions)


GRAMMAR_START_TOKEN = 'BLAZON'

# The grammar.
#
# Keys are the nonterminals in the grammar; values are GrammarNode objects
# specifying lists of expansions. Terminals are identified as tokens that are
# not keys in this dictionary; they will be emitted in the blazon.
#
# For human understandability, nonterminals are in ALL_CAPS_WITH_UNDERSCORES
# (but this is not enforced or used by the code).
#
# An expansion is a list of tokens, which may include nonterminals and/or
# terminals. An expansion can be empty.
#
# The first element in an expansion can be an integer. If so, it is treated as a
# multiplier, and that expansion (with the multiplier removed) will be added the
# corresponding number of times to the list of expansions in the GrammarNode.
# This is useful to adjust probabilities.
#
# For convenience, if an expansion is just a single token, it can be written as
# the token itself rather than a singleton list containing the token. This is
# particularly helpful in long lists of singleton expansions, such as the lists
# of charges. In those cases it also helps to work around absolutely terrible
# yapf formatting if all of the expansions are written as singleton lists; yapf
# is also agonizingly slow and gobbles hundreds of megs of RAM.
#
# The DrawShield parser is really finicky about semicolons and commas: sometimes
# they are optional, sometimes they are required in order to get the appropriate
# parse, and sometimes they are forbidden. The punctuation here is the result of
# some deep spelunking through the parser code to figure out where it should go.
GRAMMAR = {
    'BLAZON': GrammarNode('SHIELD'),
    'SHIELD': GrammarNode(
        [
            4,
            'SIMPLE',
            ';',  # First semicolon() in shield().
        ],
        [
            'SIMPLE',
            ';',  # First semicolon() in shield().
            'SHIELD_COMBINER',
            'SIMPLE',
            ';'  # Second semicolon() in shield().
        ],
        [
            'QUARTERING',
            ';'  # First semicolon() in shield().
        ]),
    'SIMPLE': GrammarNode([
        'FIELD',
        # In "Or hurty two fusils sable", without a comma, the "two" gets eaten
        # by treatment() because it's looking for a number that could be used
        # for "mulletty of n points" or "checky-of-9".
        ',',  # comma() in simple().
        'ORDINARIES',
        'CHARGES'
    ]),
    'SHIELD_COMBINER': GrammarNode('dimidiated with', 'impaled with'),
    'QUARTERING': GrammarNode([
        'quartered first',
        'SHIELD',
        'second',
        'SHIELD',
        'third',
        'SHIELD',
        'fourth',
        'SHIELD',
        ';'  # Second semicolon() in quartered().
    ]),
    'FIELD': GrammarNode('FILL', 'DIVISION'),
    'FILL': GrammarNode('TINCTURE_OR_FUR', 'TREATMENT'),
    # For now, "potent" is not included here because it can confuse the parser.
    # Examples:
    #   * "Or, a cross potent" is parsed as the charge "cross potent" missing a
    #     tincture.
    #   * "Per pall potent counter-potent and vert" is parsed as
    #     "potent-counter-potent and vert," missing a third tincture.
    # Both of these problems can be avoided by including a comma to break up the
    # phrase in the incorrect parse, so maybe with some punctuation tweaks we
    # can put potent back in. (The second case is easy, because really a comma
    # should always go between the first and second tinctures, and actually
    # that's already fixed below. The first case is trickier because in most
    # cases there shouldn't be a comma between an ordinary and its tincture.)
    # But for the purposes of blazonzip, potent is effectively the same as
    # counter-potent, so we aren't missing much by leaving it out.
    #
    # For a similar reason, "potent-counter-potent" can confuse the parser:
    #   * "Or, a cross potent-counter-potent" is parsed as "Or, a cross potent
    #     counter-potent".
    # So "potent-counter-potent" is also not included here.
    'TINCTURE_OR_FUR': GrammarNode(
        'argent', 'azure', 'bisque', 'bronze', 'brunatre', 'buff', 'carnation',
        'celestial azure', 'cendree', 'copper', 'counter-ermine',
        'counter-potent', 'counter-vair', 'crimson', 'ermine', 'erminites',
        'erminois', 'gules', 'iron', 'lead', 'murrey', 'or', 'orange', 'pean',
        'purpure', 'red-ochre', 'rose', 'sable', 'sanguine', 'senois', 'steel',
        'tenne', 'vair', 'vair-en-pale', 'vair-en-point', 'vert',
        'yellow-ochre'),
    # Treatments of two colors are written with the treatment in the middle
    # because (1) that's apparently supported and (2) the recommended syntax,
    # "<treatment> <color> and <color>," can lead to parser confusion. Examples:
    #   * Because "<color> <treatment> <color>" is allowed, "Tierced in pale
    #     argent, chequy gules and or, and sable" is parsed as "argent chequy
    #     gules," "or," and "sable."
    #   * "Tierced in pale argent hurty, potenty gules and or, and sable" is
    #     (for some reason) parsed as though "potenty" is an edge type
    #     describing the tiercing, and then DrawShield doesn't know what to do
    #     with the "sable."
    # TODO: "semy de" treatments: TINCTURE_OR_FUR + 'semy de' + CHARGE_PLURAL +
    # TINCTURE_OR_FUR. But things in ORDINARY_OR_CHARGE in DrawShield's grammar
    # don't work right (e.g., "semy de frets", "semy de chevrons").
    'TREATMENT':
    GrammarNode(['TINCTURE_OR_FUR', 'BINARY_TREATMENT', 'TINCTURE_OR_FUR'],
                ['TINCTURE_OR_FUR', 'UNARY_TREATMENT']),
    'BINARY_TREATMENT': GrammarNode(
        'annuletty', 'billetty', 'chequy', 'chequy-of-9', 'counter-billetty',
        'crusily', 'ermined', 'estoilly', 'fretty', 'goutty', 'grillage',
        'honeycombed', 'lozengy', 'maily', 'masoned', 'mulletty', 'papelonny',
        'plumetty', 'potenty', 'scaly', 'seme-de-lys', 'vairy'),
    'UNARY_TREATMENT': GrammarNode('bezanty', 'hurty', 'platy'),
    'DIVISION': GrammarNode(
        [
            'DIVISION_OF_TWO',
            'FILL',
            'and',
            'FILL',
            ';'  # semicolon() in division().
        ],
        [
            'DIVISION_OF_THREE',
            'FILL',
            ',',
            'FILL',
            'and',
            'FILL',
            ';'  # semicolon() in division().
        ]),
    'DIVISION_OF_TWO': GrammarNode(
        'per bend', 'per bend sinister', 'per chevron', 'per chevron inverted',
        'per fess', 'per pale', 'per pile', 'per saltire', 'quarterly'),
    'DIVISION_OF_THREE':
    GrammarNode('per pall', 'per pall inverted', 'per pile', 'tierced in bend',
                'tierced in bend sinister', 'tierced in chevron',
                'tierced in fess', 'tierced in pale'),
    'ORDINARIES': GrammarNode([2], ['ORDINARY_WITH_FILL', 'ORDINARIES']),
    'ORDINARY_WITH_FILL': GrammarNode(
        [
            20,
            'AN_ORDINARY',
            'FILL',
            ','  # comma() in objects().
        ],
        [
            'AN_ORDINARY_NO_FILL',
            ','  # comma() in objects().
        ]),
    # Special cases here: "two chevrons" and "three chevrons" are ordinaries
    # (more than three are interpreted instead as charges).
    'AN_ORDINARY': GrammarNode(
        ['a bar', 'COUPED_DEXTER_SINISTER_OPT'],
        ['TWO_OR_THREE', 'bars', 'COUPED_DEXTER_SINISTER_OPT'],
        ['a bar gemel', 'COUPED_DEXTER_SINISTER_OPT'],
        ['TWO_OR_THREE', 'bars gemel', 'COUPED_DEXTER_SINISTER_OPT'],
        ['a barrulet', 'COUPED_DEXTER_SINISTER_OPT'],
        ['TWO_TO_EIGHT', 'barrulets', 'COUPED_DEXTER_SINISTER_OPT'],
        ['a base', 'VOIDED_OPT'], ['a baton', 'DEXTER_OPT', 'VOIDED_OPT'],
        ['a bend', 'SINISTER_OPT', 'COUPED_OPT', 'VOIDED_OPT'],
        ['two bends', 'SINISTER_OPT'], 'a bendlet',
        ['TWO_TO_FIVE', 'bendlets'], 'a bordure',
        ['a Canadian pale', 'COUPED_BASE_CHIEF_OPT', 'VOIDED_OPT'],
        ['a canton', 'SINISTER_OPT', 'INVERTED_OPT', 'VOIDED_OPT'],
        ['a chevron', 'CHEVRON_ORIENTATION_OPT', 'VOIDED_OPT'],
        ['a chevron', 'CHEVRON_VARIANT', 'INVERTED_OPT'],
        ['a chevron burst', 'INVERTED_OPT', 'VOIDED_OPT'],
        ['a chevron removed', 'SINISTER_OPT', 'INVERTED_OPT'],
        ['TWO_OR_THREE', 'chevrons', 'INVERTED_OPT'],
        ['a chevronel', 'INVERTED_OPT'],
        ['TWO_OR_THREE', 'chevronels', 'INTERLACED_OPT'],
        ['a chief', 'COUPED_DEXTER_SINISTER_OPT'],
        ['a chief triangular', 'VOIDED_OPT'],
        ['a closet', 'COUPED_DEXTER_SINISTER_OPT'],
        ['a couple close', 'SINISTER_OPT'], ['a crancelin', 'SINISTER_OPT'],
        ['a cross', 'VOIDED_OPT'], 'a cross parted and fretty',
        'a cross pattée', ['a cross quarter pierced', 'VOIDED_OPT'],
        'a cross tripartite and fretty',
        ['a dance', 'COUPED_DEXTER_SINISTER_OPT'], 'a double tressure',
        'a double tressure flory-counter-flory',
        ['an endorse', 'COUPED_BASE_CHIEF_OPT'],
        ['TWO_TO_EIGHT', 'endorses', 'COUPED_BASE_CHIEF_OPT'],
        'an enty en point',
        ['a fess', 'COUPED_DEXTER_SINISTER_OPT', 'VOIDED_OPT'],
        ['a fess between two chevrons', 'INVERTED_OPT'],
        ['a fillet', 'VOIDED_OPT'], ['a fillet cross', 'VOIDED_OPT'],
        ['a flaunch', 'SINISTER_OPT'], ['a fret', 'COUPED_OPT'],
        ['a gore', 'SINISTER_OPT'], 'a gorge',
        ['a graft', 'SINISTER_OPT', 'INVERTED_OPT', 'VOIDED_OPT'],
        ['a gusset', 'SINISTER_OPT', 'INVERTED_OPT', 'VOIDED_OPT'],
        ['a gyron', 'SINISTER_OPT', 'INVERTED_OPT', 'VOIDED_OPT'], 'a humet',
        ['an inescutcheon', 'INVERTED_OPT'], [
            'a label entire of', 'THREE_FIVE_OR_SEVEN', 'points',
            'LABEL_POINT_SHAPE'
        ], 'a lozenge throughout', ['a mountain', 'INVERTED_OPT'], 'an orle',
        ['a pale', 'COUPED_BASE_CHIEF_OPT', 'VOIDED_OPT'],
        ['a palet', 'COUPED_BASE_CHIEF_OPT'],
        ['TWO_OR_THREE', 'palets', 'COUPED_BASE_CHIEF_OPT'],
        ['a palet gemel', 'COUPED_BASE_CHIEF_OPT'],
        ['TWO_OR_THREE', 'palets gemel', 'COUPED_BASE_CHIEF_OPT'],
        ['a pall', 'INVERTED_OPT', 'VOIDED_OPT'],
        ['a pallium', 'INVERTED_OPT'],
        ['a pile', 'PILE_VARIANT_OPT', 'INVERTED_OPT'],
        ['a point', 'POINT_LOCATION', 'VOIDED_OPT'],
        ['a quarter', 'SINISTER_OPT', 'INVERTED_OPT', 'VOIDED_OPT'],
        ['a riband', 'SINISTER_OPT'],
        ['TWO_TO_EIGHT', 'ribands', 'SINISTER_OPT'],
        ['a saltire', 'VOIDED_OPT'], 'a saltire parted and fretty', 'a scarpe',
        ['TWO_TO_FIVE', 'scarpes'],
        ['a shakefork', 'INVERTED_OPT', 'VOIDED_OPT'],
        ['a square flaunch', 'SINISTER_OPT'],
        ['a tierce', 'SINISTER_OPT', 'VOIDED_OPT'], 'a tressure',
        ['a trimount', 'INVERTED_OPT']),
    'AN_ORDINARY_NO_FILL': GrammarNode('a ford'),
    'CHEVRON_ORIENTATION_OPT':
    GrammarNode([], 'couched', 'couched sinister', 'inverted'),
    'CHEVRON_VARIANT': GrammarNode('disjointed', 'fracted', 'ploye', 'rompu'),
    'COUPED_OPT': GrammarNode([], 'couped'),
    'COUPED_BASE_CHIEF_OPT': GrammarNode([], 'couped', 'couped in base',
                                         'couped in chief'),
    'COUPED_DEXTER_SINISTER_OPT': GrammarNode([], 'couped', 'couped dexter',
                                              'couped sinister'),
    'DEXTER_OPT': GrammarNode([], 'dexter'),
    'INTERLACED_OPT': GrammarNode([], 'interlaced'),
    'INVERTED_OPT': GrammarNode([], 'inverted'),
    'PILE_VARIANT_OPT': GrammarNode([], 'embowed', 'throughout'),
    'POINT_LOCATION': GrammarNode('dexter', 'in point', 'sinister'),
    'SINISTER_OPT': GrammarNode([], 'sinister'),
    'VOIDED_OPT': GrammarNode([], 'voided'),
    'LABEL_POINT_SHAPE': GrammarNode('dovetailed', 'pattee', 'straight'),
    'CHARGES': GrammarNode(
        [2],
        [
            'CHARGE_POSITION_OPT',
            'CHARGE_ARRANGEMENT',
            ',',  # comma() in objects().
            'CHARGES'
        ]),
    # DrawShield supports only one to three charges in base, so "in base" is not
    # included here for now.
    'CHARGE_POSITION_OPT':
    GrammarNode([14], 'in abyss', 'in dexter chief', 'in dexter flank',
                'in each flank', 'in fess point', 'in honour point',
                'in middle chief', 'in nombril', 'in first quarter',
                'in second quarter', 'in third quarter', 'in fourth quarter',
                'in sinister chief', 'in sinister flank'),
    # Man, this is obnoxious. It's almost as if I'm trying to simulate a
    # context-sensitive language with a context-free grammar.
    'CHARGE_ARRANGEMENT': GrammarNode(
        [20, 'A_CHARGE', 'FILL'],
        [20, 'two', 'CHARGE_PLURAL', 'ARRANGEMENT_TWO', 'FILL'],
        [20, 'three', 'CHARGE_PLURAL', 'ARRANGEMENT_THREE', 'FILL'],
        [20, 'four', 'CHARGE_PLURAL', 'ARRANGEMENT_FOUR', 'FILL'],
        [20, 'five', 'CHARGE_PLURAL', 'ARRANGEMENT_FIVE', 'FILL'],
        [20, 'six', 'CHARGE_PLURAL', 'ARRANGEMENT_SIX', 'FILL'],
        [20, 'seven', 'CHARGE_PLURAL', 'ARRANGEMENT_SEVEN', 'FILL'],
        [20, 'eight', 'CHARGE_PLURAL', 'ARRANGEMENT_EIGHT', 'FILL'],
        [20, 'nine', 'CHARGE_PLURAL', 'ARRANGEMENT_NINE', 'FILL'],
        [20, 'ten', 'CHARGE_PLURAL', 'ARRANGEMENT_TEN', 'FILL'],
        # The semicolons after compositions hit the second semicolon() call in
        # simpleCharge().
        [10, 'three', 'CHARGE_PLURAL', 'FILL', ',', 'COMPOSITION_THREE',';' ],
        [10, 'four', 'CHARGE_PLURAL', 'FILL', ',', 'COMPOSITION_FOUR',';'],
        [10, 'five', 'CHARGE_PLURAL', 'FILL', ',', 'COMPOSITION_FIVE',';'],
        [10, 'six', 'CHARGE_PLURAL', 'FILL', ',', 'COMPOSITION_SIX',';'],
        [10, 'seven', 'CHARGE_PLURAL', 'FILL', ',', 'COMPOSITION_SEVEN',';'],
        [10, 'eight', 'CHARGE_PLURAL', 'FILL', ',', 'COMPOSITION_EIGHT',';'],
        [10, 'nine', 'CHARGE_PLURAL', 'FILL', ',', 'COMPOSITION_NINE',';'],
        [10, 'ten', 'CHARGE_PLURAL', 'FILL', ',', 'COMPOSITION_TEN',';'],
        'A_CHARGE_NO_FILL',
        ['two', 'CHARGE_PLURAL_NO_FILL', 'ARRANGEMENT_TWO'],
        ['three', 'CHARGE_PLURAL_NO_FILL', 'ARRANGEMENT_THREE'],
        ['four', 'CHARGE_PLURAL_NO_FILL', 'ARRANGEMENT_FOUR'],
        ['five', 'CHARGE_PLURAL_NO_FILL', 'ARRANGEMENT_FIVE'],
        ['six', 'CHARGE_PLURAL_NO_FILL', 'ARRANGEMENT_SIX'],
        ['seven', 'CHARGE_PLURAL_NO_FILL', 'ARRANGEMENT_SEVEN'],
        ['eight', 'CHARGE_PLURAL_NO_FILL', 'ARRANGEMENT_EIGHT'],
        ['nine', 'CHARGE_PLURAL_NO_FILL', 'ARRANGEMENT_NINE'],
        ['ten', 'CHARGE_PLURAL_NO_FILL', 'ARRANGEMENT_TEN'],
        ['three', 'CHARGE_PLURAL_NO_FILL', ',', 'COMPOSITION_THREE',';'],
        ['four', 'CHARGE_PLURAL_NO_FILL', ',', 'COMPOSITION_FOUR',';'],
        ['five', 'CHARGE_PLURAL_NO_FILL', ',', 'COMPOSITION_FIVE',';'],
        ['six', 'CHARGE_PLURAL_NO_FILL', ',', 'COMPOSITION_SIX',';'],
        ['seven', 'CHARGE_PLURAL_NO_FILL', ',', 'COMPOSITION_SEVEN',';'],
        ['eight', 'CHARGE_PLURAL_NO_FILL', ',', 'COMPOSITION_EIGHT',';'],
        ['nine', 'CHARGE_PLURAL_NO_FILL', ',', 'COMPOSITION_NINE',';'],
        ['ten', 'CHARGE_PLURAL_NO_FILL', ',', 'COMPOSITION_TEN',';']),
    'TWO_OR_THREE': GrammarNode('two', 'three'),
    'TWO_TO_FIVE': GrammarNode('two', 'three', 'four', 'five'),
    'TWO_TO_EIGHT':
    GrammarNode('two', 'three', 'four', 'five', 'six', 'seven', 'eight'),
    'TWO_TO_TEN': GrammarNode('two', 'three', 'four', 'five', 'six', 'seven',
                              'eight', 'nine', 'ten'),
    'THREE_FIVE_OR_SEVEN': GrammarNode('three', 'five', 'seven'),
    # "In chief" is an arrangement, but it doesn't play well with positions, so
    # it is not included here.
    'ARRANGEMENT_TWO': GrammarNode(
        [], 'addorsed', 'confronty', 'counter-passant', 'in bend',
        'in bend sinister', 'in fess', 'in fess throughout', 'in pale',
        'in pale throughout', 'pilewise', 'respecting each other'),
    'ARRANGEMENT_THREE': GrammarNode(
        [], 'counter-passant', 'in bend', 'in bend sinister', 'in chevron',
        'in fess', 'in fess throughout', 'in pale', 'in pale throughout',
        'in pall', 'in pile', 'pilewise'),
    'ARRANGEMENT_FOUR': GrammarNode(
        [], 'addorsed', 'counter-passant', 'in annulo', 'in a quadrangle',
        'in bend', 'in bend sinister', 'in chevron', 'in cross', 'in fess',
        'in fess throughout', 'in orle', 'in pale', 'in pale throughout',
        'in saltire', 'pilewise', 'respecting each other'),
    'ARRANGEMENT_FIVE': GrammarNode(
        [], 'counter-passant', 'in annulo', 'in bend', 'in bend sinister',
        'in chevron', 'in cross', 'in fess', 'in fess throughout', 'in pale',
        'in pale throughout', 'in saltire'),
    'ARRANGEMENT_SIX': GrammarNode(
        [], 'addorsed', 'counter-passant', 'in annulo', 'in bend',
        'in bend sinister', 'in chevron', 'in fess', 'in fess throughout',
        'in orle', 'in pale', 'in pale throughout', 'in pile',
        'respecting each other'),
    'ARRANGEMENT_SEVEN': GrammarNode([], 'in annulo', 'in fess',
                                     'in fess throughout', 'in pale',
                                     'in pale throughout'),
    'ARRANGEMENT_EIGHT': GrammarNode([], 'in annulo', 'in fess',
                                     'in fess throughout', 'in orle',
                                     'in pale', 'in pale throughout'),
    'ARRANGEMENT_NINE': GrammarNode([], 'in annulo', 'in fess',
                                    'in fess throughout', 'in pale',
                                    'in pale throughout'),
    'ARRANGEMENT_TEN': GrammarNode([], 'in annulo', 'in fess',
                                   'in fess throughout', 'in orle', 'in pale',
                                   'in pale throughout'),
    # In each of the following, three compositions are excluded: the all-ones
    # composition (which is the same as "in pale"), the composition of one part
    # (which is the same as "in fess"), and the composition corresponding to the
    # default arrangement. So COMPOSITION_n has 2^(n-1)-3 expansions.
    'COMPOSITION_THREE': GrammarNode('one and two'),
    'COMPOSITION_FOUR': GrammarNode(['one,', 'COMPOSITION_THREE'],
                                    'one, two and one', 'one and three',
                                    'two, one and one', 'two and two'),
    'COMPOSITION_FIVE': GrammarNode(
        [5, 'one,', 'COMPOSITION_FOUR'], 'one, three and one', 'one and four',
        ['two,', 'COMPOSITION_THREE'], 'two, one, one and one',
        'two, two and one', 'two and three', 'three, one and one',
        'four and one'),
    'COMPOSITION_SIX': GrammarNode(
        [13, 'one,', 'COMPOSITION_FIVE'], 'one, three and two', 'one and five',
        [5, 'two,', 'COMPOSITION_FOUR'], 'two, one, one, one and one',
        'two, three and one', 'two and four', ['three,', 'COMPOSITION_THREE'],
        'three, one, one and one', 'three and three', 'four, one and one',
        'four and two', 'five and one'),
    'COMPOSITION_SEVEN':
    GrammarNode([29, 'one,', 'COMPOSITION_SIX'], 'one, three, two and one',
                'one and six', [13, 'two,', 'COMPOSITION_FIVE'],
                'two, one, one, one, one and one', 'two, three and two',
                'two and five', [5, 'three,', 'COMPOSITION_FOUR'],
                'three, one, one, one and one', 'three and four',
                ['four,', 'COMPOSITION_THREE'], 'four, one, one and one',
                'four, two and one', 'four and three', 'five, one and one',
                'five and two', 'six and one'),
    'COMPOSITION_EIGHT': GrammarNode(
        [61, 'one,', 'COMPOSITION_SEVEN'], 'one, three, three and two',
        'one and seven', [29, 'two,', 'COMPOSITION_SIX'],
        'two, one, one, one, one, one and one', 'two, three, two and one',
        'two and six', [13, 'three,', 'COMPOSITION_FIVE'],
        'three, one, one, one, one and one', 'three and five',
        [5, 'four,', 'COMPOSITION_FOUR'], 'four, one, one, one and one',
        'four, three and one', 'four and four', ['five,', 'COMPOSITION_THREE'],
        'five, one, one and one', 'five, two and one', 'five and three',
        'six, one and one', 'six and two', 'seven and one'),
    # COMPOSITION_SIX includes "three and three", so COMPOSITION_NINE includes
    # "three, three and three", which is the default arrangement. Oh well.
    'COMPOSITION_NINE': GrammarNode(
        [125, 'one,', 'COMPOSITION_EIGHT'], 'one, three, three, and two',
        'one and eight', [61, 'two,', 'COMPOSITION_SEVEN'],
        'two, one, one, one, one, one, one and one',
        'two, three, three and one', 'two and seven',
        [29, 'three,', 'COMPOSITION_SIX'],
        'three, one, one, one, one, one and one', 'three, three, two and one',
        'three and six', [13, 'four,', 'COMPOSITION_FIVE'],
        'four, one, one, one, one and one', 'four, three and two',
        'four and five', [5, 'five,', 'COMPOSITION_FOUR'],
        'five, one, one, one and one', 'five, three and one', 'five and four',
        ['six,', 'COMPOSITION_THREE'], 'six, one, one and one',
        'six, two and one', 'six and three', 'seven, one and one',
        'seven and two', 'eight and one'),
    'COMPOSITION_TEN': GrammarNode(
        [254, 'one,', 'COMPOSITION_NINE'], 'one and nine',
        [125, 'two,', 'COMPOSITION_EIGHT'],
        'two, one, one, one, one, one, one, one and one',
        'two, three, three, and two', 'two and eight',
        [61, 'three,', 'COMPOSITION_SEVEN'],
        'three, one, one, one, one, one, one and one',
        'three, three, three and one', 'three and seven',
        [29, 'four,', 'COMPOSITION_SIX'],
        'four, one, one, one, one, one and one', 'four and six',
        [13, 'five,', 'COMPOSITION_FIVE'], 'five, one, one, one, one and one',
        'five, three and two', 'five and five',
        [5, 'six,', 'COMPOSITION_FOUR'], 'six, one, one, one and one',
        'six, three and one', 'six and four', ['seven,', 'COMPOSITION_THREE'],
        'seven, one, one and one', 'seven, two and one', 'seven and three',
        'eight, one and one', 'eight and two', 'nine and one'),
    # For now, "escutcheon" is not included as a charge because it hits a
    # special case in the parser: simpleCharge() calls shield() for it, which
    # means that something like "an escutcheon argent;" will have the semicolon
    # eaten by the shield() call. Example: "Quartered, first quartered, first
    # gules; second or; third argent; fourth vert an escutcheon argent; ; second
    # sable; third azure; fourth or" triggers a "Duplicate quarter" error from
    # DrawShield, because the shield() call after "escutcheon" eats the first of
    # the following semicolons and the parser gets confused about the nested
    # quartering. This example actually needs *three* semicolons after "an
    # escutcheon argent".
    #
    # Special cases here (see also the special cases for the plural list below):
    #  *  "two annulets braced", "two annulets conjunct", "two annulets
    #     interlaced", "three annulets interlaced", "two flails in saltire",
    #     "two palm leaves in saltire", and "two palm trees in saltire". These
    #     are included here because they must appear with the specified number
    #     (and they act as a single charge).
    #  *  "chevrons" are charges when there are more than three, but ordinaries
    #     otherwise; "chevrons couped" forces the charge interpretation, so that
    #     is what is used here.
    #  *  "saltires" are charges, but "a saltire" is an ordinary; "a saltire
    #     couped" forces the charge interpretation. So it's "a saltire couped"
    #     here, but just "saltires" in the plural list.
    #  *  "crosses pattée" are charges, but "a cross pattée" is an ordinary;
    #     "crosslet formy" forces the charge interpretation, so that's how they
    #     are called in both lists. (Note "formy", "pattée", "paty" are
    #     synonymous.)
    'A_CHARGE': GrammarNode(* [
        'an abacus', 'an acanthus leaf', 'an accordion', 'an acorn',
        'an acorn sprig', 'an addice', 'an alchemical symbol for air',
        'an alchemical symbol for copper', 'an alchemical symbol for earth',
        'an alchemical symbol for fire', 'an alchemical symbol for gold',
        'an alchemical symbol for iron', 'an alchemical symbol for mercury',
        'an alchemical symbol for salt', 'an alchemical symbol for sulphur',
        'an alchemical symbol for tin', 'an alchemical symbol for water',
        'an alder tree', 'an alerion', 'an alphyn passant',
        'an alphyn rampant', 'an alquerque board', 'an altar', 'an amphiptere',
        'an amphora', 'an anchor', 'an angel', 'an anille', 'an ankh',
        'an annulet', 'an annulet of chain', 'an annulet of rope',
        'two annulets braced', 'two annulets conjunct',
        'two annulets interlaced', 'three annulets interlaced', 'an ant',
        'an anteater rampant', 'an antelope', 'an antelope passant',
        'an antelope rampant', 'an antique crown', 'an antler', 'an anvil',
        'an apple', 'an apple tree', 'an arch', 'an archer', 'an arm couped',
        'an arm embowed', 'an arm armoured embowed', 'an armadillo',
        'an arrow', "an artist's brush", 'an ash sprig',
        'an ash tree eradicated', 'an aspen tree', 'an ass', "an ass's head",
        'an astral crown', 'an astrolabe', 'an astronomical symbol for Earth',
        'an astronomical symbol for Jupiter',
        'an astronomical symbol for Mars', 'an astronomical symbol for Mercury',
        'an astronomical symbol for Neptune',
        'an astronomical symbol for Pluto', 'an astronomical symbol for Saturn',
        'an astronomical symbol for the moon',
        'an astronomical symbol for the sun',
        'an astronomical symbol for Uranus',
        'an astronomical symbol for Venus', 'an auk', 'an awl', 'an axe',
        'an axe double headed', 'a badge of clan Ross', 'a badger',
        'a badger rampant', "a badger's head", "a badger's head erased",
        'a bag', 'a bagpipe', 'a bagwyn', 'a ball of yarn', 'a bamboo',
        'a banana stem', 'a barbel', "a barber's pole", "a baronet's helmet",
        'a baronial crown', 'a barrel', 'a bascule', 'a bat',
        'a battering ram', 'a battle axe', 'a beacon', 'a beanpod',
        'a bear couchant', 'a beard', 'a bear dormant', 'a bear passant',
        'a bear passant guardant', 'a bear rampant', "a bear's head",
        "a bear's head caboshed", "a bear's head erased", 'a beaver',
        'a beaver rampant', 'a bee', 'a beech sprig', 'a beehive', 'a beetle',
        'a bell', 'a bellows', 'a billet', 'a billhook', 'a birch tree',
        'a biscione', "a bison's head caboshed", 'a blackbird',
        'a blackbird displayed', 'a blacksmith', 'a boar courant',
        'a boar rampant', "a boar's head", 'a boar statant', 'a boar winged',
        'a bookshelf', 'a boot', 'a bow', 'a Bowen knot', 'a brey', 'a bridge',
        'a bridge of three arches', 'a bridge of two arches', 'a broad arrow',
        'a broadaxe', 'a bucket', 'a buckle', 'a bugle horn', 'a bull',
        'a bull passant', 'a bull rampant', "a bull's head", 'a bull winged',
        'a bulrush', 'a bunch of grapes', 'a bundle of arrows',
        'a bundle of cramps', 'a bundle of reeds', 'a bundle of tulips',
        'a bush', 'a butter churn', 'a butterfly', 'a caduceus', 'a calamarie',
        'a calf', 'a calipers', 'a caltrap', 'a Calvary cross', 'a camail',
        'a camel', 'a camelopard', 'a camelopard dormant',
        'a camelopard rampant', 'a camelopard statant', 'a camelopard winged',
        'a camel rampant', 'a candlestick', 'a cap of Mercury', 'a caravel',
        'a carbuncle', 'a carp embowed', "a carpenter's square", 'a carrot',
        'a cartouche', 'a castle', 'a castle triple towered', 'a cat',
        'a catamount', "a catamount's head", 'a cat couchant',
        'a cat couchant guardant', 'a catfish embowed', 'a Catherine wheel',
        'a cat herisse', 'a cat passant', 'a cat passant guardant',
        'a cat rampant', 'a cat sejant', 'a cauldron', 'a cedar tree',
        'a Celtic cross', 'a Celtic knot', 'a centaur', 'a chabot', 'a chain',
        'a chaine shot', 'a chair', 'a chalice', 'a chatloup', 'a cheese wedge',
        'a cheese wheel', 'a cherry sprig', 'a cherub', 'a chess bishop',
        'a chess castle', 'a chess king', 'a chess knight', 'a chess pawn',
        'a chess queen', 'a chess rook', 'a chestnut tree', 'a chevalier',
        'a chevron couped', 'a Chinese dragon', 'a Chinese dragon in annulo',
        'a Chi Rho', 'a chough', 'a chrysanthemum', 'a chrysanthemum leaf',
        'a church', 'a cinquefoil', 'a clarion', 'a claymore', 'a closed book',
        'a closed modern book', 'a cloud', 'a club', 'a cock', 'a cockatrice',
        'a cockatrice sejant', 'a cogwheel', 'a columbine', 'a comedy mask',
        'a comet', 'a compass', 'a compass rose', 'a compass star',
        'a compass star pierced', 'a compass star voided', 'a coney',
        'a coney couchant', 'a coney courant', 'a coney forepaw raised',
        'a coney rampant', 'a coney salient', 'a coney sejant erect',
        'a conker', 'a Coptic cross', 'a coracle', 'a cord', 'a cornflower',
        'a cornice', 'a corn sheaf', 'a cornucopia', 'a cow', "a cow's head",
        'a crab', 'a cramp', 'a crane', 'a crane holding a stone',
        'a crescent', 'a crescent pendent', 'a crevice', 'a cricket',
        'a crocodile', 'a crook of Basel', 'a cross avellane',
        'a cross avellane fitchy', 'a cross avellane pierced', 'a cross barby',
        'a cross botonny', 'a cross botonny fitchy', 'a cross botonny Latin',
        'a cross botonny pierced', 'a cross botonny voided', 'a crossbow',
        'a cross cercelée', 'a cross cercelée pierced',
        'a cross cercelée voided', 'a cross cleche', 'a cross cleche fitchy',
        'a cross cleche Latin', 'a cross cleche pierced',
        'a cross cleche voided', 'a cross couped', 'a cross crescenty',
        'a cross crosslet', 'a cross crosslet crossed',
        'a cross crosslet crossed fitchy', 'a cross crosslet crossed pierced',
        'a cross crosslet crossed voided', 'a cross crosslet fitchy',
        'a cross crosslet pierced', 'a cross crosslet voided', 'a cross fitchy',
        'a cross fitchy double', 'a cross fitchy double pierced',
        'a cross fitchy double voided', 'a cross fitchy pierced',
        'a cross fitchy voided', 'a cross floretty', 'a cross floretty fitchy',
        'a cross floretty pierced', 'a cross floretty voided', 'a cross flory',
        'a cross flory fitchy', 'a cross flory Latin',
        'a cross flory of nine fusils', 'a cross flory of nine fusils fitchy',
        'a cross flory of nine fusils pierced', 'a cross flory of one lozenge',
        'a cross flory of one lozenge fitchy',
        'a cross flory of one lozenge pierced', 'a cross flory pierced',
        'a cross flory voided', 'a cross fourche', 'a cross fourche fitchy',
        'a cross fourche pierced', 'a cross fourche voided', 'a crosslet',
        'a crosslet fitchy', 'a crosslet formy', 'a crosslet formy fitchy',
        'a crosslet formy pierced', 'a crosslet pierced', 'a crosslet voided',
        'a cross moline', 'a cross moline fitchy', 'a cross moline pierced',
        'a cross of four fusils', 'a cross of four fusils fitchy',
        'a cross of four fusils pierced', 'a cross of Lorraine',
        'a cross of nine fusils', 'a cross of nine fusils fitchy',
        'a cross of nine fusils pierced', 'a cross of Saint Florian',
        'a cross patoncé', 'a cross patoncé fitchy', 'a cross patoncé pierced',
        'a cross patoncé voided', 'a cross paty floretty',
        'a cross paty floretty fitchy', 'a cross paty floretty pierced',
        'a cross paty floretty voided', 'a cross paty pointed',
        'a cross paty pointed fitchy', 'a cross paty pointed pierced',
        'a cross paty pointed voided', 'a cross pointed',
        'a cross pointed fitchy', 'a cross pointed pierced',
        'a cross pointed voided', 'a cross pommé', 'a cross pommé fitchy',
        'a cross pommé pierced', 'a cross pommé voided', 'a cross portate',
        'a cross potent', 'a cross potent crossed',
        'a cross potent crossed fitchy', 'a cross potent crossed pierced',
        'a cross potent crossed voided', 'a cross potent fitchy',
        'a cross potent pierced', 'a cross potent voided',
        'a cross recercelée', 'a cross sarcelly', 'a cross sarcelly fitchy',
        'a cross sarcelly pierced', 'a cross sarcelly voided', 'a crow',
        'a crow essorant', 'a crown palisado', 'a crown vallary',
        "a crow's head", 'a crozier', 'a crystal', 'a cushion', 'a cutlass',
        'a cypress tree', 'a daffodil', 'a dagger', 'a daisy',
        'a dandelion leaf', 'a deer', 'a de Lacy knot', 'a delf', 'a devil',
        'a die', 'a doe lodged', 'a doe rampant', 'a doe trippant', 'a dog',
        "a dog's head", 'a dolphin', 'a domino', 'a double arch', 'a dove',
        'a dove essorant', 'a dove fondant', 'a dove volant',
        'a dragon displayed', 'a dragon dormant', 'a dragonfly',
        'a dragon in annulo', 'a dragon passant', 'a dragon rampant',
        'a dragon sejant affronty', 'a drum', 'a drumstick', 'a ducal crown',
        'a duck', "a duke's crown", 'an eagle closed', 'an eagle displayed',
        'an eagle double headed', 'an eagle rising elevated', "an eagle's head",
        "an earl's crown", 'an ear of wheat', 'an eastern crown', 'an eel',
        'an eel basket', 'an Egyptian cross', 'an Egyptian sphinx',
        'an elder sprig', 'an elder sprig fructed', 'an elephant',
        'an elephant head', 'an elephant maintaining a tower', 'an elk lodged',
        'an elk rampant', 'an elk trippant', "an embroiderer's broach",
        'an ermine', 'an ermine spot', 'an escallop', "an esquire's helmet",
        'a ewer', 'an eye', 'a falchion', 'a falcon', 'a fasces', 'a feather',
        'a field piece', 'a fig leaf', 'a fig tree', 'a fireball', 'a fir tree',
        'a fish', 'a fishhook', 'a fishing boat', 'a fishtail gryphon',
        'a flail', 'two flails in saltire', 'a flame', 'a flask', 'a fleam',
        'a fleece', 'a fleshpot', 'a fleur-de-lys', 'a flint stone', 'a flute',
        'a fly', 'a foot', 'a footprint', 'a forge hammer', 'a forget-me-not',
        'a fountain stone', 'a fox', 'a fox courant', 'a foxglove',
        'a fox sejant', "a fox's head", 'a frauenadler', 'a frog',
        'a frog rampant', 'a frog salient', 'a frog sejant',
        'a frog sejant affronty', 'a fusil', 'a fylfot clubbed', 'a galley',
        'a garlic plant', 'a garter', 'a gate', 'a gauntlet',
        'a gauntlet clenched', 'a gillyflower', 'a glove', 'a goat',
        'a goat clymant', "a goat's head", 'a goldfish', 'a gonfannon',
        'a goose', 'a gorilla', 'a goutte', 'a Greek cross', 'a Greek sphinx',
        'a Greek sphinx sejant', 'a Greek sphinx statant guardant',
        'a grape leaf', 'a greyhound', 'a greyhound courant',
        'a greyhound rampant', 'a gryphon', 'a gryphon passant',
        "a gryphon's head", 'a gull', 'a gull volant', 'a hackle', 'a hammer',
        'a hand', 'a hand clawed', 'a hand of benediction', 'a harp',
        'a harpy', 'a harpy rising', 'a harrow', 'a hautboy', "a hawk's bell",
        "a hawk's lure", 'a hazelnut', 'a heart', 'a helmet',
        'a helmet closed', 'a hen', "a herald's staff", 'a herring',
        'a hexagon', 'a hexagon pierced', 'a hexagon voided', 'a hind',
        'a hippocampus', 'a hoe', 'a holly leaf', 'a holly sprig',
        'a hop cone', 'a hornet', 'a horse passant', 'a horse rampant',
        'a horse salient', "a horse's head", 'a horseshoe', 'a hunting horn',
        'a hurst', 'a hydra', 'an ibex', 'an icicle', 'an ivy leaf',
        'an ivy vine', 'an ivy wreath', 'a jasmine flower', 'a Jerusalem cross',
        "a jester's bauble", "a jester's cap", 'a kangaroo salient', 'a key',
        'a keythong', 'a King of Arms crown', 'a knot', [
            'a label of ', 'THREE_FIVE_OR_SEVEN', 'points', 'LABEL_POINT_SHAPE'
        ], 'a labyrinth', 'a ladder', 'a lamb', 'a lamia', 'a lamp', 'a lark',
        'a lark displayed', 'a lark essorant', 'a laurel leaf',
        'a laurel wreath', 'a laurel wreath fructed', 'a leather bottle',
        'a leek', 'a leg', 'a lemon', "a leopard's head",
        "a leopard's head jessant-de-lys", 'a leveret courant',
        'a leveret salient', 'a leveret sejant', 'a leveret statant',
        'a lighthouse', 'a lightning bolt', 'a lily', 'a lime leaf',
        'a linden leaf', 'a linden tree eradicated', 'a lion cadent',
        'a lion couchant', 'a lion courant', 'a lion coward', 'a lion dormant',
        'a lion passant', 'a lion passant guardant',
        'a lion passant reguardant', 'a lion rampant',
        'a lion rampant guardant', 'a lion rampant reguardant',
        'a lion rampant tail forked', 'a lion rampant tail forked saltire',
        'a lion rampant tail forked saltire reversed',
        'a lion rampant tail nowed', 'a lion salient', 'a lion sejant',
        'a lion sejant erect', "a lion's head affronty", "a lion's head erased",
        "a lion's head guardant", "a lion's head reguardant", 'a lion statant',
        'a lion statant guardant', 'a lizard rampant', 'a Latin cross',
        'a loaf', 'a lotus', "a lover's knot", 'a lozenge',
        'a lozenge pierced', 'a lozenge voided', 'a luce', 'a lynx passant',
        'a lynx rampant', 'a lyre', 'a mace', 'a madonna', 'a magpie',
        'a magpie volant en arriere', "a maiden's head",
        "a maiden's head and bust", "a maiden's head and bust nude",
        'a mailed fist', 'a mallet', 'a Maltese cross', 'a mammoth',
        'a manatee', "a man's head", "a man's head and bust", 'a manticore',
        'a maple leaf', 'a marijuana leaf', "a marquis' crown", 'a martlett',
        'a mascle', 'a mascle ploye', 'a mattock', 'a maunche', 'a meat spit',
        'a menorah of nine', 'a menorah of seven', 'a mermaid',
        'a mermaid in her modesty', 'a mermaid in her vanity', 'a merman',
        'a millrind', 'a millstone', 'a millwheel', 'a mistral', 'a mitre',
        'a mjolnir', 'a moldiwarp', 'a monkey', "a monkey's head",
        'a monkey winged passant', 'a monkey winged rising', 'a moon',
        'a moon crescent', 'a moon decrescent', 'a moon increscent',
        "a Moor's head", 'a moorcock', 'a morion', 'a mortar and pestle',
        'a mouse couchant', 'a mouse passant', 'a mouse rampant', 'a mug',
        'a mullet', 'a mullet of eight points',
        'a mullet of eight points pierced',
        'a mullet of eight points voided interlaced',
        'a mullet of four points', 'a mullet of four points pierced',
        'a mullet of nine points', 'a mullet of nine points pierced',
        'a mullet of seven points', 'a mullet of seven points pierced',
        'a mullet of seven points voided interlaced', 'a mullet of six points',
        'a mullet of six points pierced', 'a mullet of ten points',
        'a mullet of ten points pierced', 'a mullet pierced',
        'a mullet voided interlaced', 'a mural crown', 'a mushroom',
        'a musical note', 'a natural dolphin', 'a natural eagle',
        'a natural fountain', 'a natural leopard', 'a natural pelican',
        'a natural rose', 'a natural tiger', 'a naval crown', 'a needle',
        'a needle threaded', 'a nesselblatt', 'a nightingale',
        "a nine men's morris board", 'a Norse longship', 'a Norse raven',
        'an oak', 'an oak leaf', 'an oak sprig', 'an octagon', 'an octofoil',
        'an olive sprig', 'an onion', 'an open book', 'an open modern book',
        'an orb', 'an orca', 'an organ pipe', 'an ostrich',
        'an ostrich feather', 'an otter', 'an ouroboros', 'an owl',
        'an owl affronty', 'an owl displayed', 'an ox', 'a padlock',
        'a pair of breeches', 'a pair of cherries', 'a pair of hands',
        'a pair of shears', 'a pair of wings', 'a pair of wings conjoined',
        'a pair of wings conjoined in lure', 'a pair of wings in lure',
        'a palm leaf', 'two palm leaves in saltire', 'a palm tree',
        'two palm trees in saltire', 'a panther', 'a panther sejant',
        'a parachute', 'a paschal lamb', 'a passion nail',
        'a paternoster cross', 'a patriarchal cross', 'a pavilion',
        'a peacock', 'a pear', "a peer's helmet", 'a pegasus',
        'a pegasus segreant', 'a pelican', 'a penguin', 'a pennon',
        'a Penrose triangle', 'a pentacle', 'a pentagon', 'a pentagon pierced',
        'a pentagon voided', 'a pentagram', 'a peony', 'a pheon', 'a phoenix',
        'a phoenix double headed', 'a Phrygian cap', 'a pickaxe', 'a pillar',
        'a pineapple', 'a pine cone', 'a pine tree', 'a pine tree eradicated',
        'a plane tree', 'a plough', 'a ploughshare', 'a plummet',
        'a polar bear', 'a poleaxe', 'a polypus', 'a pomegranate',
        'a popinjay', 'a poppy', 'a porcupine', 'a portcullis', 'a pumpkin',
        'a pyramid', 'a python erect affronty', 'a python volant', 'a quail',
        'a quatrefoil', 'a quatrefoil knot', 'a question mark', 'a quill pen',
        'a radish', 'a ragged staff', 'a rainbow', 'a ram', 'a rapier', 'a rat',
        'a raven', 'a raven displayed', 'a raven migrant', 'a raven volant',
        'a recorder', 'a reed', 'a reindeer', 'a reindeer rampant',
        'a reindeer salient', "a reindeer's head", 'a reindeer trippant',
        'a rhinoceros', 'a ribbon', 'a rock', 'a rod of Asclepius', 'a rosary',
        'a rose', 'a roundel', 'a royal crown', 'a royal helmet',
        'a rubber duck', 'a rudder', 'a rue flower', 'a rue sprig',
        'a Russian cross', 'a rustre', 'a saddle', 'a salamander', 'a salmon',
        'a salmon spear', 'a saltcellar', 'a saltire couped', 'a saltorel',
        "a Saracen's head", 'a savage', "a savage's head", 'a Saxon crown',
        'a scissors', 'a scorpion', 'a scythe', 'a sea dog', 'a sea dragon',
        'a sea goat', 'a sea horse', 'a sea lion', 'a sea lion embowed',
        'a sea serpent', 'a sea tortoise', 'a sea wolf', 'a seax', 'a serpent',
        'a serpent erect', 'a serpent erect tail nowed', 'a serpent nowed',
        'a set of scales', 'a sexfoil', 'a shackle', 'a shacklebolt',
        'a shamrock', 'a shark', 'a sheep', "a shepherd's crook", 'a ship',
        "a ship's wheel", 'a shirt of mail', 'a shofar', 'a shrimp',
        'a shuttle', 'a sickle', 'a skeleton', 'a skull', 'a slaughter axe',
        'a sledgehammer', 'a slingshot', 'a snail', 'a snake',
        'a snake glissant', 'a snake passant', 'a snowflake',
        'a soldering iron', 'a spade', 'a spear', 'a sphinx', 'a spider',
        'a spiderweb', 'a spool of thread', 'a spoon', 'a spur', 'a square',
        'a square pierced', 'a square voided', 'a squirrel rampant',
        'a squirrel sejant', 'a squirrel sejant holding a nut',
        'a squirrel statant', 'a staff', 'a Stafford knot', 'a stag at gaze',
        'a stag beetle', 'a stag courant', 'a stag lodged', 'a stag passant',
        'a stag rampant', "a stag's head", "a stag's horn", 'a stag trippant',
        'a staple', 'a star', 'a starling', 'a star of David', 'a stirrup',
        'a stone', 'a strawberry', 'a strawberry flower', 'a strawberry leaf',
        'a strawberry plant', 'a sun', 'a sunburst', 'a sunflower',
        'a swallow', 'a swan', 'a swan displayed', 'a swan essorant',
        'a swan nageant', 'a swan passant', 'a swan rising', 'a swan rousant',
        'a swan volant', 'a sword', 'a swordfish', 'a sword of Saint Paul',
        'a table', 'a talbot passant', 'a talbot rampant', 'a talbot sejant',
        'a talbot statant', 'a tassel', 'a tau cross', 'a thimble',
        'a thistle', 'a throwing axe', 'a thunderbolt', 'a thyrsus',
        'a tiger passant', 'a tilting lance', 'a toad', 'a torch', 'a torque',
        'a tortoise', 'a tower', 'a tower triple towered', 'a tragedy mask',
        'a tree', 'a tree blasted eradicated', 'a tree eradicated',
        'a trefoil', 'a trestle', 'a triangle', 'a triangle pierced',
        'a triangle voided', 'a trident', 'a trillium', 'a Trinity knot',
        'a triskele', 'a triskelion', 'a triskelion armoured',
        'a triskelion of spirals', "a Triton's trumpet", 'a trivet',
        'a trumpet', 'a truncheon', 'a Tudor rose', 'a tulip', 'a turkeycock',
        'a turner axe', 'a turnip', 'a turtle', "a twelve men's morris board",
        'a Ukrainian trident head', 'a unicorn', 'a unicorn couchant',
        'a unicorn passant', 'a unicorn rampant', "a unicorn's head couped",
        "a unicorn's head erased", 'a unicorn statant', 'a unicorn trippant',
        'an urcheon', 'a valknut', 'a vine', 'a vine leaf', 'a violin',
        "a viscount's crown", 'a vulture', 'a vulture displayed', 'a wagon',
        'a Wake knot', 'a walnut', 'a wasp', 'a water bouget', 'a waterwheel',
        'a well', 'a Welsh dragon', 'a Welsh dragon rampant', 'a whale',
        'a whale embowed', 'a whale embowed spouting', 'a whale spouting',
        'a wheat stalk', 'a wheel', 'a whelk', 'a wicker fence',
        'a willow tree', 'a windmill', 'a wing', 'a wolf', 'a wolf courant',
        'a wolf iron', 'a wolf passant', 'a wolf rampant', 'a wolf reguardant',
        'a wolf salient', "a wolf's head affronty", "a wolf's head erased",
        'a wolf statant', 'a wreathed staff', 'a wyvern', 'a wyvern displayed',
        'a wyvern rampant', 'a wyvern sejant displayed',
        'a wyvern sejant forepaw raised', 'a wyvern sejant',
        'a wyvern tail nowed', 'a yale rampant', 'a yew tree', 'a zebra',
        'a zodiac symbol for Aquarius', 'a zodiac symbol for Aries',
        'a zodiac symbol for Cancer', 'a zodiac symbol for Capricorn',
        'a zodiac symbol for Gemini', 'a zodiac symbol for Leo',
        'a zodiac symbol for Libra', 'a zodiac symbol for Pisces',
        'a zodiac symbol for Sagittarius', 'a zodiac symbol for Scorpio',
        'a zodiac symbol for Taurus', 'a zodiac symbol for Virgo'
    ]),
    # Special cases here (see also the special cases for the singular list
    # above):
    #  *  "annulets concentric" must be preceded by a number between two and ten
    #     (inclusive), so there is no corresponding singular entry.
    #  *  "frets" and "shakeforks" are charges, but "a fret" or "a shakefork" is
    #     an ordinary, so there are no corresponding singular entries.
    #
    # "Crosses couped" is not included here because DrawShield apparently
    # doesn't know how to draw more than one of those.
    #
    # A plural charge that starts with a number word (e.g., "nine men's morris
    # boards", "twelve men's morris boards") can confuse the parser. For
    # example, in the blazon "Argent, eleven torteaux, two nine men's morris
    # boards tenne", the parser interprets "two nine" as the arrangement of the
    # eleven torteaux (two and nine) and then can't understand "men's morris
    # boards". (This is apparently true even if the arrangement doesn't make
    # sense for the preceding number of charges, e.g., "Argent, three torteaux,
    # six nine men's morris boards": here "six and nine" can't be an arrangement
    # for three torteaux, but the parser doesn't know that.) So these aren't
    # included here.
    'CHARGE_PLURAL': GrammarNode(* [
        'abaci', 'acanthus leaves', 'accordions', 'acorns', 'acorn sprigs',
        'addices', 'alchemical symbols for air',
        'alchemical symbols for copper', 'alchemical symbols for earth',
        'alchemical symbols for fire', 'alchemical symbols for gold',
        'alchemical symbols for iron', 'alchemical symbols for mercury',
        'alchemical symbols for salt', 'alchemical symbols for sulphur',
        'alchemical symbols for tin', 'alchemical symbols for water',
        'alder trees', 'alerions', 'alphyns passant', 'alphyns rampant',
        'alquerque boards', 'altars', 'amphipteres', 'amphorae', 'anchors',
        'angels', 'anilles', 'ankhs', 'annulets', 'annulets concentric',
        'annulets of chain', 'annulets of rope', 'anteaters rampant',
        'antelopes', 'antelopes passant', 'antelopes rampant',
        'antique crowns', 'antlers', 'ants', 'anvils', 'apples', 'apple trees',
        'archers', 'arches', 'armadillos', 'arms couped', 'arms embowed',
        'arms armoured embowed', 'arrows', "artist's brushes", 'ash sprigs',
        'ash trees eradicated', 'aspen trees', 'asses', "ass's heads",
        'astral crowns', 'astrolabes', 'astronomical symbols for Earth',
        'astronomical symbols for Jupiter', 'astronomical symbols for Mars',
        'astronomical symbols for Mercury', 'astronomical symbols for Neptune',
        'astronomical symbols for Pluto', 'astronomical symbols for Saturn',
        'astronomical symbols for the moon',
        'astronomical symbols for the sun', 'astronomical symbols for Uranus',
        'astronomical symbols for Venus', 'auks', 'awls', 'axes',
        'axes double headed', 'badges of clan Ross', 'badgers',
        "badger's heads", "badger's heads erased", 'badgers rampant',
        'bagpipes', 'bagwyns', 'bags', 'balls of yarn', 'bamboos',
        'banana stems', 'barbels', "barber's poles", "baronet's helmets",
        'baronial crowns', 'barrels', 'bascules', 'bats', 'battering rams',
        'battle axes', 'beacons', 'beanpods', 'beards', 'bears couchant',
        'bears dormant', "bear's heads", "bear's heads caboshed",
        "bear's heads erased", 'bears passant', 'bears passant guardant',
        'bears rampant', 'beavers', 'beavers rampant', 'beech sprigs',
        'beehives', 'bees', 'beetles', 'bellows', 'bells', 'billets',
        'billhooks', 'birch trees', 'bisciones', "bison's heads caboshed",
        'blackbirds', 'blackbirds displayed', 'blacksmiths', 'boars courant',
        "boar's heads", 'boars rampant', 'boars statant', 'boars winged',
        'bookshelves', 'boots', 'Bowen knots', 'bows', 'breys', 'bridges',
        'bridges of three arches', 'bridges of two arches', 'broad arrows',
        'broadaxes', 'buckets', 'buckles', 'bugle horns', 'bulls',
        "bull's heads", 'bulls passant', 'bulls rampant', 'bulls winged',
        'bulrushes', 'bunches of grapes', 'bundles of arrows',
        'bundles of cramps', 'bundles of reeds', 'bundles of tulips', 'bushes',
        'butter churns', 'butterflies', 'caducei', 'calamaries', 'calipers',
        'caltraps', 'Calvary crosses', 'calves', 'camails', 'camelopards',
        'camelopards dormant', 'camelopards rampant', 'camelopards statant',
        'camelopards winged', 'camels', 'camels rampant', 'candlesticks',
        'caps of Mercury', 'caravels', 'carbuncles', 'carp embowed',
        "carpenter's squares", 'carrots', 'cartouches', 'castles',
        'castles triple towered', 'catamounts', "catamount's heads",
        'catfishes embowed', 'Catherine wheels', 'cats', 'cats couchant',
        'cats couchant guardant', 'cats herisse', 'cats passant guardant',
        'cats passant', 'cats rampant', 'cats sejant', 'cauldrons',
        'cedar trees', 'Celtic crosses', 'Celtic knots', 'centaurs', 'chabots',
        'chains', 'chaine shots', 'chairs', 'chalices', 'chatloups',
        'cheese wedges', 'cheese wheels', 'cherry sprigs', 'cherubim',
        'chess bishops', 'chess castles', 'chess kings', 'chess knights',
        'chess pawns', 'chess queens', 'chess rooks', 'chestnut trees',
        'chevaliers', 'chevrons couped', 'Chinese dragons',
        'Chinese dragons in annulo', 'Chi Rhos', 'choughs',
        'chrysanthemum leaves', 'chrysanthemums', 'churches', 'cinquefoils',
        'clarions', 'claymores', 'closed books', 'closed modern books',
        'clouds', 'clubs', 'cockatrices', 'cockatrices sejant', 'cocks',
        'cogwheels', 'columbines', 'comedy masks', 'comets', 'compasses',
        'compass roses', 'compass stars', 'compass stars pierced',
        'compass stars voided', 'coneys', 'coneys couchant', 'coneys courant',
        'coneys forepaws raised', 'coneys rampant', 'coneys salient',
        'coneys sejant erect', 'conkers', 'Coptic crosses', 'coracles', 'cords',
        'cornflowers', 'cornices', 'corn sheaves', 'cornucopias', 'cows',
        "cow's heads", 'crabs', 'cramps', 'cranes', 'cranes holding stones',
        'crescents', 'crescents pendent', 'crevices', 'crickets', 'crocodiles',
        'crooks of Basel', 'crossbows', 'crosses avellane',
        'crosses avellane fitchy', 'crosses avellane pierced', 'crosses barby',
        'crosses botonny', 'crosses botonny fitchy', 'crosses botonny Latin',
        'crosses botonny pierced', 'crosses botonny voided',
        'crosses cercelée', 'crosses cercelée pierced',
        'crosses cercelée voided', 'crosses cleche', 'crosses cleche fitchy',
        'crosses cleche Latin', 'crosses cleche pierced',
        'crosses cleche voided', 'crosses crescenty', 'crosses crosslet',
        'crosses crosslet crossed', 'crosses crosslet crossed fitchy',
        'crosses crosslet crossed pierced', 'crosses crosslet crossed voided',
        'crosses crosslet fitchy', 'crosses crosslet pierced',
        'crosses crosslet voided', 'crosses fitchy', 'crosses fitchy double',
        'crosses fitchy double pierced', 'crosses fitchy double voided',
        'crosses fitchy pierced', 'crosses fitchy voided', 'crosses floretty',
        'crosses floretty fitchy', 'crosses floretty pierced',
        'crosses floretty voided', 'crosses flory', 'crosses flory fitchy',
        'crosses flory Latin', 'crosses flory of nine fusils',
        'crosses flory of nine fusils fitchy',
        'crosses flory of nine fusils pierced', 'crosses flory of one lozenge',
        'crosses flory of one lozenge fitchy',
        'crosses flory of one lozenge pierced', 'crosses flory pierced',
        'crosses flory voided', 'crosses fourche', 'crosses fourche fitchy',
        'crosses fourche pierced', 'crosses fourche voided', 'crosses moline',
        'crosses moline fitchy', 'crosses moline pierced',
        'crosses of four fusils', 'crosses of four fusils fitchy',
        'crosses of four fusils pierced', 'crosses of Lorraine',
        'crosses of nine fusils', 'crosses of nine fusils fitchy',
        'crosses of nine fusils pierced', 'crosses of Saint Florian',
        'crosses patoncé', 'crosses patoncé fitchy', 'crosses patoncé pierced',
        'crosses patoncé voided', 'crosses paty floretty',
        'crosses paty floretty fitchy', 'crosses paty floretty pierced',
        'crosses paty floretty voided', 'crosses paty pointed',
        'crosses paty pointed fitchy', 'crosses paty pointed pierced',
        'crosses paty pointed voided', 'crosses pointed',
        'crosses pointed fitchy', 'crosses pointed pierced',
        'crosses pointed voided', 'crosses pommé', 'crosses pommé fitchy',
        'crosses pommé pierced', 'crosses pommé voided', 'crosses portate',
        'crosses potent', 'crosses potent crossed',
        'crosses potent crossed fitchy', 'crosses potent crossed pierced',
        'crosses potent crossed voided', 'crosses potent fitchy',
        'crosses potent pierced', 'crosses potent voided',
        'crosses recercelée', 'crosses sarcelly', 'crosses sarcelly fitchy',
        'crosses sarcelly pierced', 'crosses sarcelly voided', 'crosslets',
        'crosslets fitchy', 'crosslets formy fitchy',
        'crosslets formy pierced', 'crosslets pierced', 'crosslets voided',
        'crowns palisado', 'crowns vallary', 'crows', 'crows essorant',
        "crow's heads", 'croziers', 'crystals', 'cushions', 'cutlasses',
        'cypress trees', 'daffodils', 'daggers', 'daisies', 'dandelion leaves',
        'deer', 'de Lacy knots', 'delves', 'devils', 'dice', 'does lodged',
        'does rampant', 'does trippant', 'dogs', "dog's heads", 'dolphins',
        'dominoes', 'double arches', 'doves', 'doves essorant',
        'doves fondant', 'doves volant', 'dragonflies', 'dragons displayed',
        'dragons dormant', 'dragons in annulo', 'dragons passant',
        'dragons rampant', 'dragons sejant affronty', 'drums', 'drumsticks',
        'ducal crowns', 'ducks', "duke's crowns", 'eagles closed',
        'eagles displayed', 'eagles double headed', 'eagles rising elevated',
        "eagle's heads", "earl's crowns", 'ears of wheat', 'eastern crowns',
        'eel baskets', 'eels', 'Egyptian crosses', 'Egyptian sphinxes',
        'elder sprigs', 'elder sprigs fructed', 'elephant heads', 'elephants',
        'elephants maintaining towers', 'elk lodged', 'elk rampant',
        'elk trippant', "embroiderer's broaches", 'ermines', 'ermine spots',
        'escallops', "esquire's helmets", 'ewers', 'eyes', 'falchions',
        'falcons', 'fasces', 'feathers', 'feet', 'field pieces', 'fig leaves',
        'fig trees', 'fireballs', 'fir trees', 'fishes', 'fishhooks',
        'fishing boats', 'fishtail gryphons', 'flails', 'flames', 'flasks',
        'fleams', 'fleeces', 'fleshpots', 'fleurs-de-lys', 'flies',
        'flint stones', 'flutes', 'footprints', 'forge hammers',
        'forget-me-nots', 'fountain stones', 'foxes', 'foxes courant',
        'foxes sejant', 'foxgloves', "fox's heads", 'frauenadlers', 'frets',
        'frogs', 'frogs rampant', 'frogs salient', 'frogs sejant',
        'frogs sejant affronty', 'fusils', 'fylfots clubbed', 'galleys',
        'garlic plants', 'garters', 'gates', 'gauntlets', 'gauntlets clenched',
        'geese', 'gillyflowers', 'gloves', 'goats', 'goats clymant',
        "goat's heads", 'goldfishes', 'gonfannons', 'gorillas', 'gouttes',
        'Greek crosses', 'Greek sphinxes', 'Greek sphinxes sejant',
        'Greek sphinxes statant guardant', 'grape leaves', 'greyhounds',
        'greyhounds courant', 'greyhounds rampant', 'gryphons',
        "gryphon's heads", 'gryphons passant', 'gulls', 'gulls volant',
        'hackles', 'hammer', 'hands', 'hands clawed', 'hands of benediction',
        'harpies', 'harpies rising', 'harps', 'harrows', 'hautboys',
        "hawk's bell", "hawk's lures", 'hazelnuts', 'hearts', 'helmets',
        'helmets closed', 'hens', "herald's staves", 'herrings', 'hexagons',
        'hexagons pierced', 'hexagons voided', 'hinds', 'hippocampi', 'hoes',
        'holly leaves', 'holly sprigs', 'hop cones', 'hornets', "horse's heads",
        'horseshoes', 'horses passant', 'horses rampant', 'horses salient',
        'hursts', 'hydras', 'ibexes', 'icicles', 'ivy leaves', 'ivy vines',
        'ivy wreaths', 'jasmine flowers', 'Jerusalem crosses',
        "jester's baubles", "jester's caps", 'kangaroos salient', 'keys',
        'keythongs', 'King of Arms crowns', 'knots', [
            'labels of', 'THREE_FIVE_OR_SEVEN', 'points', 'LABEL_POINT_SHAPE'
        ], 'labyrinths', 'ladders', 'lambs', 'lamias', 'lamps', 'larks',
        'larks displayed', 'larks essorant', 'laurel leaves', 'laurel wreaths',
        'laurel wreaths fructed', 'leather bottles', 'leeks', 'legs', 'lemons',
        "leopard's heads", "leopard's heads jessant-de-lys",
        'leverets courant', 'leverets salient', 'leverets sejant',
        'leverets statant', 'lighthouses', 'lightning bolts', 'lilies',
        'lime leaves', 'linden leaves', 'linden trees eradicated',
        'lions cadent', 'lions couchant', 'lions courant', 'lions coward',
        'lions dormant', "lion's heads affronty", "lion's heads erased",
        "lion's heads guardant", "lion's heads reguardant", 'lions passant',
        'lions passant guardant', 'lions passant reguardant', 'lions rampant',
        'lions rampant guardant', 'lions rampant reguardant',
        'lions rampant tail forked', 'lions rampant tail forked saltire',
        'lions rampant tail forked saltire reversed',
        'lions rampant tail nowed', 'lions salient', 'lions sejant',
        'lions sejant erect', 'lions statant', 'lions statant guardant',
        'lizards rampant', 'Latin crosses', 'loaves', 'lotuses',
        "lover's knots", 'lozenges', 'lozenges pierced', 'lozenges voided',
        'luces', 'lynxes passant', 'lynxes rampant', 'lyres', 'maces',
        'madonnas', 'magpies', 'magpies volant en arriere', "maiden's heads",
        "maiden's heads and busts", "maiden's heads and busts nude",
        'mailed fists', 'mallets', 'Maltese crosses', 'mammoths', 'manatees',
        "man's heads", "man's heads and busts", 'manticores', 'maple leaves',
        'marijuana leaves', "marquis' crowns", 'martletts', 'mascles',
        'mascles ploye', 'mattocks', 'maunches', 'meat spits',
        'menorahs of nine', 'menorahs of seven', 'mermaids',
        'mermaids in their modesty', 'mermaids in their vanity', 'mermen',
        'mice couchant', 'mice passant', 'mice rampant', 'millrinds',
        'millstones', 'millwheels', 'mistrals', 'mitres', 'mjolnirs',
        'moldiwarps', 'monkeys', "monkey's heads", 'monkeys winged passant',
        'monkeys winged rising', 'moons', 'moons crescent', 'moons decrescent',
        'moons increscent', "Moor's heads", 'moorcocks', 'morions',
        'mortars and pestles', 'mugs', 'mullets', 'mullets of eight points',
        'mullets of eight points pierced',
        'mullets of eight points voided interlaced', 'mullets of four points',
        'mullets of four points pierced', 'mullets of nine points',
        'mullets of nine points pierced', 'mullets of seven points',
        'mullets of seven points pierced',
        'mullets of seven points voided interlaced', 'mullets of six points',
        'mullets of six points pierced', 'mullets of ten points',
        'mullets of ten points pierced', 'mullets pierced',
        'mullets voided interlaced', 'mural crowns', 'mushrooms',
        'musical notes', 'natural dolphins', 'natural eagles',
        'natural fountains', 'natural leopards', 'natural pelicans',
        'natural roses', 'natural tigers', 'naval crowns', 'needles',
        'needles threaded', 'nesselblatts', 'nightingales', 'Norse longships',
        'Norse ravens', 'oaks', 'oak leaves', 'oak sprigs', 'octagons',
        'octofoils', 'olive sprigs', 'onions', 'open books', 'open modern books',
        'orbs', 'orcas', 'organ pipes', 'ostriches', 'ostrich feathers',
        'otters', 'ouroboroi', 'owls', 'owls affronty', 'owls displayed',
        'oxen', 'padlocks', 'pairs of breeches', 'pairs of cherries',
        'pairs of hands', 'pairs of shears', 'pairs of wings',
        'pairs of wings conjoined', 'pairs of wings conjoined in lure',
        'pairs of wings in lure', 'palm leaves', 'palm trees', 'panthers',
        'panthers sejant', 'parachutes', 'paschal lambs', 'passion nails',
        'paternoster crosses', 'patriarchal crosses', 'pavilions', 'peacocks',
        'pears', "peer's helmets", 'pegasi', 'pegasi segreant', 'pelicans',
        'penguins', 'pennons', 'Penrose triangles', 'pentacles', 'pentagons',
        'pentagons pierced', 'pentagons voided', 'pentagrams', 'peonies',
        'pheons', 'phoenices', 'phoenixes double headed', 'Phrygian caps',
        'pickaxes', 'pillars', 'pineapples', 'pine cones', 'pine trees',
        'pine trees eradicated', 'plane trees', 'ploughs', 'ploughshares',
        'plummets', 'polar bears', 'poleaxes', 'polypuses', 'pomegranates',
        'popinjays', 'poppies', 'porcupines', 'portcullises', 'pumpkins',
        'pyramids', 'pythons erect affronty', 'pythons volant', 'quail',
        'quatrefoil knots', 'quatrefoils', 'question marks', 'quill pens',
        'radishes', 'ragged staves', 'rainbows', 'rams', 'rapiers', 'rats',
        'ravens', 'ravens displayed', 'ravens migrant', 'ravens volant',
        'recorders', 'reeds', 'reindeer', 'reindeer lodged', 'reindeer rampant',
        'reindeer salient', "reindeer's heads", 'reindeer trippant',
        'rhinoceroses', 'ribbons', 'rocks', 'rods of Asclepius', 'rosaries',
        'roses', 'roundels', 'royal crowns', 'royal helmets', 'rubber ducks',
        'rudders', 'rue flowers', 'rue sprigs', 'Russian crosses', 'rustres',
        'saddles', 'salamanders', 'salmon', 'salmon spears', 'saltcellars',
        'saltires', 'saltorels', "Saracen's heads", 'savages',
        "savage's heads", 'Saxon crowns', 'scissors', 'scorpions', 'scythes',
        'sea dogs', 'sea dragons', 'sea goats', 'sea horses', 'sea lions',
        'sea lions embowed', 'sea serpents', 'sea tortoises', 'sea wolves',
        'seaxes', 'serpents', 'serpents erect', 'serpents erect tails nowed',
        'serpents nowed', 'sets of scales', 'sexfoils', 'shacklebolts',
        'shackles', 'shakeforks', 'shamrocks', 'sharks', 'sheep',
        "shepherd's crooks", 'ships', "ship's wheels", 'shirts of mail',
        'shofroth', 'shrimp', 'shuttles', 'sickles', 'skeletons', 'skulls',
        'slaughter axes', 'sledgehammers', 'slingshots', 'snails', 'snakes',
        'snakes glissant', 'snakes passant', 'snowflakes', 'soldering irons',
        'spades', 'spears', 'sphinxes', 'spiders', 'spiderwebs',
        'spools of thread', 'spoons', 'spurs', 'squares', 'squares pierced',
        'squares voided', 'squirrels rampant', 'squirrels sejant',
        'squirrels sejant holding nuts', 'squirrels statant', 'Stafford knots',
        'staves', 'stag beetles', 'stags at gaze', 'stags courant',
        "stag's heads", "stag's horns", 'stags lodged', 'stags passant',
        'stags rampant', 'stags trippant', 'staples', 'starlings', 'stars',
        'stars of David', 'stirrups', 'stones', 'strawberries',
        'strawberry flowers', 'strawberry leaves', 'strawberry plants',
        'sunbursts', 'sunflowers', 'suns', 'swallows', 'swans',
        'swans displayed', 'swans essorant', 'swans nageant', 'swans passant',
        'swans rising', 'swans rousant', 'swans volant', 'sword', 'swordfishes',
        'swords of Saint Paul', 'tables', 'talbots passant', 'talbots rampant',
        'talbots sejant', 'talbots statant', 'tassels', 'tau crosses',
        'thimbles', 'thistles', 'throwing axes', 'thunderbolts', 'thyrsi',
        'tigers passant', 'tilting lances', 'toads', 'torches', 'torques',
        'tortoises', 'towers', 'towers triple towered', 'tragedy masks',
        'trees', 'trees blasted eradicated', 'trees eradicated', 'trefoils',
        'trestles', 'triangles', 'triangles pierced', 'triangles voided',
        'tridents', 'trilliums', 'Trinity knots', 'triskeles', 'triskelions',
        'triskelions armoured', 'triskelions of spirals', "Triton's trumpets",
        'trivets', 'trumpets', 'truncheons', 'Tudor roses', 'tulips',
        'turkeycocks', 'turner axes', 'turnips', 'turtles',
        'Ukrainian trident heads', 'unicorns', 'unicorns couchant',
        "unicorn's heads couped", "unicorn's heads erased", 'unicorns passant',
        'unicorns rampant', 'unicorns statant', 'unicorns trippant', 'urcheons',
        'valknuts', 'vines', 'vine leaves', 'violins', "viscount's crowns",
        'vultures', 'vultures displayed', 'wagons', 'Wake knots', 'walnuts',
        'wasps', 'water bougets', 'waterwheels', 'wells', 'Welsh dragons',
        'Welsh dragons rampant', 'whales', 'whales embowed',
        'whales embowed spouting', 'whales spouting', 'wheat stalks', 'wheels',
        'whelks', 'wicker fences', 'willow trees', 'windmills', 'wings',
        'wolf irons', "wolf's heads affronty", "wolf's heads erased", 'wolves',
        'wolves courant', 'wolves passant', 'wolves rampant',
        'wolves reguardant', 'wolves salient', 'wolves statant',
        'wreathed staves', 'wyverns', 'wyverns displayed', 'wyverns rampant',
        'wyverns sejant displayed', 'wyverns sejant forepaws raised',
        'wyverns sejant', 'wyverns tails nowed', 'yales rampant', 'yew trees',
        'zebras', 'zodiac symbols for Aquarius', 'zodiac symbols for Aries',
        'zodiac symbols for Cancer', 'zodiac symbols for Capricorn',
        'zodiac symbols for Gemini', 'zodiac symbols for Leo',
        'zodiac symbols for Libra', 'zodiac symbols for Pisces',
        'zodiac symbols for Sagittarius', 'zodiac symbols for Scorpio',
        'zodiac symbols for Taurus', 'zodiac symbols for Virgo'
    ]),
    'A_CHARGE_NO_FILL': GrammarNode(
        'a bezant', 'a fountain', 'a golpe', "a goutte d'eau",
        'a goutte de larmes', 'a goutte de poix', 'a goutte de sang',
        'a goutte de vin', "a goutte d'huile", "a goutte d'olive",
        "a goutte d'or", 'a guze', 'a hurt', 'an orange', 'a pellet',
        'a plate', 'a pomme', 'a torteau'),
    'CHARGE_PLURAL_NO_FILL': GrammarNode(
        'bezants', 'fountains', 'golpes', "gouttes d'eau", 'gouttes de larmes',
        'gouttes de poix', 'gouttes de sang', 'gouttes de vin',
        "gouttes d'huile", "gouttes d'olive", "gouttes d'or", 'guzes', 'hurts',
        'oranges', 'pellets', 'plates', 'pommes', 'torteaux'),
}
